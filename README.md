# SSRS Reporting #
This is a web-application based on SQL Service Reporting Services with load of reports.

Databases' schemas are declared in Visual Studio Database projects. Reports are using defined stored procedures as data resources. That allows to incapsulate data extracting algorithms from report representation.

Developer environment: **T-SQL, SQL Server, Visual Studio**.

Used:

 * **SQL Server Reporting Services**;
 * SQL Server 2005.