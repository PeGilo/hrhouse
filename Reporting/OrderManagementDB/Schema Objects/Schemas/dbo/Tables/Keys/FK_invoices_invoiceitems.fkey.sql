﻿ALTER TABLE [dbo].[invoiceitems]  WITH CHECK ADD  CONSTRAINT [FK_invoices_invoiceitems] FOREIGN KEY([invoicenum], [customerpin], [anno])
REFERENCES [dbo].[invoices] ([invoicenum], [customerpin], [anno])


GO
ALTER TABLE [dbo].[invoiceitems] CHECK CONSTRAINT [FK_invoices_invoiceitems]

