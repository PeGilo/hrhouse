﻿CREATE TABLE [dbo].[joinAdmGroupInvoice] (
    [AdmPin]         VARCHAR (8)    NOT NULL,
    [GroupPin]       VARCHAR (8)    NOT NULL,
    [PaymentPercent] NUMERIC (5, 2) NULL,
    [SuperGroupId]   INT            NULL
);

