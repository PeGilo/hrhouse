﻿CREATE TABLE [dbo].[joinSuperGroup] (
    [id]             INT           IDENTITY (1, 1) NOT NULL,
    [RagioneSociale] VARCHAR (100) NULL,
    [PI]             CHAR (50)     NULL,
    [aggregate]      TINYINT       NULL,
    [SGpin]          CHAR (8)      NULL
);

