CREATE TABLE [invoiceitems](
	[invoicenum] [int] NOT NULL,
	[customerpin] [varchar](8) NOT NULL,
	[id] [int] IDENTITY(1,1) NOT NULL,
	[description] [varchar](100) NULL,
	[qta] [money] NULL,
	[cost] [money] NULL,
	[sumofcost] [money] NULL,
	[notVat] [money] NULL,
	[anno] [char](4) NOT NULL,
	[ConfLater] [money] NULL ,
	[RapidoPreno] [money] NULL ,
	[VV-lisa] [money] NULL,
	[travelMoney] [money] NULL,
	[locationPin] [varchar](8) NULL,
	[idAppo] [int] NULL,
	[vat22] [float] NULL,
	[vat23] [float] NULL,
	[vat8] [float] NULL,
	[vat9] [float] NULL,
	[vat12] [float] NULL,
	[vat13] [float] NULL,
	[foodVat22] [float] NULL,
	[foodVat13] [float] NULL
) ON [PRIMARY]





