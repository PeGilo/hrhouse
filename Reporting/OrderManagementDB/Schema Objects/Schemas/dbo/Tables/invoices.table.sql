CREATE TABLE [dbo].[invoices](
	[invoicenum] [int] NOT NULL,
	[customerpin] [varchar](8) NOT NULL,
	[data] [datetime] NULL,
	[customer] [varchar](8) NULL,
	[importo] [money] NULL,
	[vat] [int] NULL,
	[frominv] [bit] NULL,
	[dateini] [datetime] NULL,
	[dateend] [datetime] NULL,
	[anno] [char](4) NOT NULL,
	[viitenumero] [varchar](19) NULL,
	[datePlay] [datetime] NULL,
	[dateDelay] [datetime] NULL,
	[created] [datetime] NULL,
	[invIdentifier] [varchar](150) NULL,
	[sentToAmica] [datetime] NULL,
	[vat22] [float] NULL,
	[vat23] [float] NULL,
	[vat8] [float] NULL,
	[vat9] [float] NULL,
	[vat12] [float] NULL,
	[vat13] [float] NULL,
	[foodVat22] [float] NULL,
	[foodVat13] [float] NULL,
	[InternationalRef] [varchar](40) NULL,
 CONSTRAINT [PK_invoices] PRIMARY KEY CLUSTERED 
(
	[invoicenum] ASC,
	[customerpin] ASC,
	[anno] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]





