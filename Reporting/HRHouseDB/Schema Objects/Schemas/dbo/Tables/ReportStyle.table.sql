﻿/*
The ReportStyle table is way for you to define multiple styles (templates) that you can switch, 
if you want, on the fly. In our organisation we have multiple brands that we report on; 
each brand can have its own styling and use a single report to do so.
*/
CREATE TABLE [dbo].[ReportStyle]
(
	ReportStyleId INT IDENTITY (1, 1),
    StyleName VARCHAR (80)
)
