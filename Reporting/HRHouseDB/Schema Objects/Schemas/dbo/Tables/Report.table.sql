﻿CREATE TABLE [dbo].[Report] (
    [Id]          INT            IDENTITY (1, 1) NOT NULL,
    [ReportPath]  NVARCHAR (500) NULL,
    [ReportName]  NVARCHAR (250) NULL,
    [Description] NVARCHAR (500) NULL
);

