﻿/*
To define a report style, you insert values into the ReportStyleElements table, 
specifying any elements  for which you want alternative styling (different from the default value). 
*/
CREATE TABLE [dbo].[ReportStyleElements]
(
	ReportStyleId INT, 
    ElementId INT,
    ElementValue VARCHAR (80)
)
