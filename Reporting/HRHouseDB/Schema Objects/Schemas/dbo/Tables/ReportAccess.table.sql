﻿CREATE TABLE [dbo].[ReportAccess] (
    [Id]       INT           IDENTITY (1, 1) NOT NULL,
    [UserId]   uniqueidentifier NOT NULL,
    [ReportId] INT           NOT NULL,
    FOREIGN KEY ([UserId]) REFERENCES [dbo].[aspnet_Users] ([UserId]) ON DELETE NO ACTION ON UPDATE NO ACTION
);

