﻿CREATE TABLE [dbo].[ReportName] (
    [Id]          INT            IDENTITY (1, 1) NOT NULL,
    [ReportId]    INT            NOT NULL,
    [Language]    NVARCHAR (5)   NULL,
    [ReportName]  NVARCHAR (250) NULL,
    [Description] NVARCHAR (500) NULL
);

