﻿CREATE TABLE [dbo].[LanguageText] (
    [Id]         INT            IDENTITY (1, 1) NOT NULL,
    [Key]        NVARCHAR (50)  NULL,
    [Language]   NVARCHAR (5)   NULL,
    [CustomerId] NVARCHAR (50)  NULL,
    [Text]       NVARCHAR (255) NULL
);

