﻿/*
The Element table holds a row for each 'stylable' element on your reports; 
a default value will be stored against the element so that you don’t have to 
specify all the different elements when you define a report style. 
*/
CREATE TABLE [dbo].[Element]
(
	ElementId INT IDENTITY (1, 1),
    ElementName VARCHAR (80),
    ElementDefaultValue VARCHAR (80)
)
