﻿CREATE FUNCTION [dbo].[F_END_OF_MONTH]
( @DAY datetime )
RETURNS  datetime
AS
/*
	Function: F_END_OF_MONTH
	Finds start of last day of month 
	for input datetime, @DAY.
	Valid for all SQL Server datetimes.
*/
BEGIN

	RETURN  DBO.F_END_OF_DAY(dateadd(mm,datediff(mm,-1,@DAY),-1))

END