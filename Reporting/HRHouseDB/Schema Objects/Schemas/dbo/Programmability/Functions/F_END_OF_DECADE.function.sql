﻿CREATE FUNCTION [dbo].[F_END_OF_DECADE]
( @DAY datetime )
RETURNS  datetime
AS
/*
	Function: F_END_OF_DECADE
	Finds start of last day of decade 
	for input datetime, @DAY.
	Valid for all SQL Server datetimes
*/
BEGIN

	RETURN   DBO.F_END_OF_DAY(dateadd(yy,9-(year(@day)%10),dateadd(yy,datediff(yy,-1,@DAY),-1)))

END