﻿CREATE FUNCTION [dbo].[F_END_OF_YEAR]
( @DAY datetime )
RETURNS  datetime
AS
/*
	Function: F_END_OF_YEAR
	Finds start of last day of year 
	for input datetime, @DAY.
	Valid for all SQL Server datetimes.
*/
BEGIN

	RETURN  DBO.F_END_OF_DAY(dateadd(yy,datediff(yy,-1,@DAY),-1))

END
