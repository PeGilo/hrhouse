﻿CREATE FUNCTION [dbo].[F_GetDateRanges]
(
	@ReportDateFrom DATETIME, 
	@ReportDateTo DATETIME, 
	@ReportPeriodType TINYINT,
	@Scope NVARCHAR (MAX)
)
RETURNS @returntable TABLE 
(
	Begindate datetime,
	Enddate datetime
)
AS
BEGIN

	DECLARE @StartPeriod datetime, @EndPeriod datetime
	DECLARE @PeriodAnount int
	
	-- Day
	IF (@ReportPeriodType = 0)
	BEGIN
	
		SET @StartPeriod = dbo.F_START_OF_DAY(@ReportDateFrom)
		SET @EndPeriod = dbo.F_END_OF_DAY(@ReportDateTo)
		SET @PeriodAnount = DATEDIFF(DAY, @ReportDateFrom, @ReportDateTo)
		
		INSERT INTO @returntable
		SELECT 
			dbo.F_START_OF_DAY(DATEADD(DAY, n.NUMBER, @StartPeriod)) as Begindate,
			dbo.F_END_OF_DAY(DATEADD(DAY, n.NUMBER, @StartPeriod)) as Enddate		
		FROM dbo.F_TABLE_NUMBER_RANGE(0, @PeriodAnount) n
			INNER JOIN dbo.F_MULTIVALUE_PARAM(@Scope, ',') k ON n.number = CONVERT(INT, k.Param) 
		
	END  -- Month
	ELSE IF (@ReportPeriodType = 1)
	BEGIN 
	
		SET @StartPeriod = dbo.F_START_OF_MONTH(@ReportDateFrom)
		SET @EndPeriod = dbo.F_END_OF_MONTH(@ReportDateTo)
		SET @PeriodAnount = DATEDIFF(MONTH, @ReportDateFrom, @ReportDateTo)
		
		INSERT INTO @returntable
		SELECT 
			dbo.F_START_OF_MONTH(DATEADD(MONTH, n.NUMBER, @StartPeriod)) as Begindate,
			dbo.F_END_OF_MONTH(DATEADD(MONTH, n.NUMBER, @StartPeriod)) as Enddate		
		FROM dbo.F_TABLE_NUMBER_RANGE(0, @PeriodAnount) n
			INNER JOIN dbo.F_MULTIVALUE_PARAM(@Scope, ',') k ON n.number = CONVERT(INT, k.Param) 
		  
	END  -- Year
	ELSE IF (@ReportPeriodType = 2)
	BEGIN
		
		SET @StartPeriod = dbo.F_START_OF_YEAR(@ReportDateFrom)
		SET @EndPeriod = dbo.F_END_OF_YEAR(@ReportDateTo)
		SET @PeriodAnount = DATEDIFF(YEAR, @ReportDateFrom, @ReportDateTo)
		
		INSERT INTO @returntable
		SELECT 
			dbo.F_START_OF_YEAR(DATEADD(YEAR, n.NUMBER, @StartPeriod)) as Begindate,
			dbo.F_END_OF_YEAR(DATEADD(YEAR, n.NUMBER, @StartPeriod)) as Enddate		
		FROM dbo.F_TABLE_NUMBER_RANGE(0, @PeriodAnount) n
			INNER JOIN dbo.F_MULTIVALUE_PARAM(@Scope, ',') k ON n.number = CONVERT(INT, k.Param) 
		
	END	

	RETURN
		
END