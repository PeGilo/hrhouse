﻿CREATE FUNCTION [dbo].[F_GetProfessionFieldTranslation] 
(@Language nvarchar(5))
RETURNS @trans TABLE  (PRFI_ID	INT, LOCALIZED_NAME	NVARCHAR(MAX))
AS
BEGIN

	IF (@Language IS NULL)  SELECT @Language = 'en-US'
		
	Declare @PreferedLanguageId INT
	select @PreferedLanguageId = LANGUAGE_ID 
	from [$(RecruitmentDatabaseName)].dbo.REP_LANGUAGE where [LANGUAGE_LOCALE_LANGUAGE] = SUBSTRING(@Language, 1, 2)
	
	Declare @DefaultLanguageId INT
	select @DefaultLanguageId = LANGUAGE_ID 
	from [$(RecruitmentDatabaseName)].dbo.REP_LANGUAGE where [LANGUAGE_LOCALE_LANGUAGE] = SUBSTRING('fi-FI', 1, 2)
	
	INSERT  @trans
	select PROF.PRFI_ID as 'PRFI_ID', 
			COALESCE(					-- Selects first non-null value
				PROF_LANG.PRFL_TEXT,	-- First select prefered language
				PROF_LANG2.PRFL_TEXT,	-- In case of no translate, select default language
				PROF.PRFI_NAME			-- In case of no language text select Name field
			) AS 'LOCALIZED_NAME'
	from [$(RecruitmentDatabaseName)].dbo.REP_PRFI_PROFESSION_FIELD as PROF
	left outer join [$(RecruitmentDatabaseName)].dbo.REP_PRFL_PROFESSION_FIELD_LANG as PROF_LANG 
		ON PROF_LANG.PRFL_PROFESSION_FIELD_ID = PROF.PRFI_ID AND PROF_LANG.PRFL_LANG_ID = @PreferedLanguageId
	left outer join [$(RecruitmentDatabaseName)].dbo.REP_PRFL_PROFESSION_FIELD_LANG as PROF_LANG2
		ON PROF_LANG2.PRFL_PROFESSION_FIELD_ID = PROF.PRFI_ID AND PROF_LANG2.PRFL_LANG_ID = @DefaultLanguageId
	RETURN
END
