﻿CREATE FUNCTION [dbo].[F_GetDateRangePoints]
(
	@ReportDateFrom DATETIME, 
	@ReportDateTo DATETIME, 
	@ReportPeriodType TINYINT,
	@Language NVARCHAR (5)
)
RETURNS @returntable TABLE 
(
	[Key] int, 
	[Value] nvarchar(255)
)
AS
BEGIN
	DECLARE @PeriodAnount int
	-- Day
	IF (@ReportPeriodType = 0)
	BEGIN
	
		SET @PeriodAnount = DATEDIFF(DAY, @ReportDateFrom, @ReportDateTo)
		
		INSERT @returntable
		SELECT n.NUMBER, dbo.F_LOCALIZE_DAY(dbo.F_START_OF_DAY(DATEADD(DAY, n.NUMBER, dbo.F_START_OF_DAY(@ReportDateFrom))), @Language)
		FROM dbo.F_TABLE_NUMBER_RANGE(0, @PeriodAnount) n
		
	END  -- Month
	ELSE IF (@ReportPeriodType = 1)
	BEGIN 
	
		SET @PeriodAnount = DATEDIFF(MONTH, @ReportDateFrom, @ReportDateTo)
		
		INSERT @returntable
		SELECT n.NUMBER, dbo.F_LOCALIZE_MONTH(dbo.F_START_OF_MONTH(DATEADD(MONTH, n.NUMBER, dbo.F_START_OF_MONTH(@ReportDateFrom))), @Language)
		FROM dbo.F_TABLE_NUMBER_RANGE(0, @PeriodAnount) n
		  
	END  -- Year
	ELSE IF (@ReportPeriodType = 2)
	BEGIN
		
		SET @PeriodAnount = DATEDIFF(YEAR, @ReportDateFrom, @ReportDateTo)
		
		INSERT @returntable 
		SELECT n.NUMBER, dbo.F_START_OF_YEAR(DATEADD(YEAR, n.NUMBER, dbo.F_START_OF_YEAR(@ReportDateFrom)))
		FROM dbo.F_TABLE_NUMBER_RANGE(0, @PeriodAnount) n
		
	END	
	
	RETURN 
	
END