﻿CREATE FUNCTION [dbo].[F_END_OF_CENTURY]
( @DAY datetime )
RETURNS  datetime
AS
/*
	Function: F_END_OF_CENTURY
	Finds start of last day of century 
	for input datetime, @DAY.
	Valid for all SQL Server datetimes
*/
BEGIN

	RETURN   DBO.F_END_OF_DAY(dateadd(yy,99-(year(@day)%100),dateadd(yy,datediff(yy,-1,@DAY),-1)))

END