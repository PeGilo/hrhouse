﻿CREATE FUNCTION [dbo].[F_MULTIVALUE_PARAM]
(@RepParam NVARCHAR (MAX), @Delim CHAR (1)=',')
RETURNS 
    @VALUES TABLE (
        [Param] NVARCHAR (MAX) NULL)
AS
BEGIN
   DECLARE @chrind INT
   DECLARE @Piece nvarchar(MAX)
   SELECT @chrind = 1
   WHILE @chrind > 0
      BEGIN
         SELECT @chrind = CHARINDEX(@Delim,@RepParam)
         IF @chrind > 0
            SELECT @Piece = LEFT(@RepParam,@chrind - 1)
         ELSE
            SELECT @Piece = @RepParam
         INSERT @VALUES(Param) VALUES(@Piece)
         SELECT @RepParam = RIGHT(@RepParam,LEN(@RepParam) - @chrind)
         IF LEN(@RepParam) = 0 BREAK
      END
   RETURN
END


