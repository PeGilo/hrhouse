﻿CREATE FUNCTION [dbo].[F_LOCALIZE_MONTH]
(
	@Date datetime,
	@Language NVARCHAR (5)
)
RETURNS NVARCHAR (255)
AS
BEGIN
	
	DECLARE @result NVARCHAR (255)
	
	SELECT @result = [Text] + ' ' + CONVERT(NVARCHAR (4), DATEPART(year, @Date))  
	FROM dbo.LanguageText 
	WHERE [Key] = 'ShortMonth_' + CONVERT(NVARCHAR (2), DATEPART(month, @Date)) AND [Language] = @Language
	
	RETURN @result
	
END