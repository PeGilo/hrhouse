﻿CREATE FUNCTION [dbo].[F_LOCALIZE_DAY]
(
	@Date datetime,
	@Language NVARCHAR (5)
)
RETURNS NVARCHAR (255)
AS
BEGIN
	
	DECLARE @result NVARCHAR (255)
	
	SELECT @result = CONVERT(NVARCHAR (2), DATEPART(day, @Date)) + ', ' + [Text]
	FROM dbo.LanguageText 
	WHERE [Key] = 'ShortMonth_' + CONVERT(NVARCHAR (2), DATEPART(month, @Date)) AND [Language] = @Language
	
	RETURN @result
	
END