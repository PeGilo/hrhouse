﻿CREATE FUNCTION [dbo].[F_END_OF_QUARTER]
( @DAY datetime )
RETURNS  datetime
AS
/*
	Function: F_END_OF_QUARTER
	Finds start of last day of quarter 
	for input datetime, @DAY.
	Valid for all SQL Server datetimes.
*/
BEGIN

	RETURN   DBO.F_END_OF_DAY(dateadd(qq,datediff(qq,-1,@DAY),-1))

END