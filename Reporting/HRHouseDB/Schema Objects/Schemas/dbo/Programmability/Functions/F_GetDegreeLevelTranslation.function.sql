﻿CREATE FUNCTION [dbo].[F_GetDegreeLevelTranslation] 
(@Language nvarchar(5))
RETURNS @trans TABLE  (DELE_ID	INT, LOCALIZED_NAME	NVARCHAR(MAX))
AS
BEGIN

	IF (@Language IS NULL)  SELECT @Language = 'en-US'
		
	Declare @PreferedLanguageId INT
	select @PreferedLanguageId = LANGUAGE_ID 
	from [$(RecruitmentDatabaseName)].dbo.REP_LANGUAGE where [LANGUAGE_LOCALE_LANGUAGE] = SUBSTRING(@Language, 1, 2)
	
	Declare @DefaultLanguageId INT
	select @DefaultLanguageId = LANGUAGE_ID 
	from [$(RecruitmentDatabaseName)].dbo.REP_LANGUAGE where [LANGUAGE_LOCALE_LANGUAGE] = SUBSTRING('fi-FI', 1, 2)
	
	INSERT  @trans
	select DL.DELE_ID as 'DELE_ID', 
			COALESCE(				-- Selects first non-null value
				DL_LANG.DELL_TEXT,  -- First select prefered language
				DL_LANG2.DELL_TEXT, -- In case of no translate, select default language
				DL.DELE_NAME        -- In case of no language text select Name field
			) AS 'LOCALIZED_NAME'
	from [$(RecruitmentDatabaseName)].dbo.REP_DELE_DEGREE_LEVEL as DL
	left outer join [$(RecruitmentDatabaseName)].dbo.REP_DELL_DEGREE_LEVEL_LANG as DL_LANG 
		ON DL_LANG.DELL_DEGREE_LEVEL_ID = DL.DELE_ID AND DL_LANG.DELL_LANG_ID = @PreferedLanguageId
	left outer join [$(RecruitmentDatabaseName)].dbo.REP_DELL_DEGREE_LEVEL_LANG as DL_LANG2 
		ON DL_LANG2.DELL_DEGREE_LEVEL_ID = DL.DELE_ID AND DL_LANG2.DELL_LANG_ID = @DefaultLanguageId

	RETURN
END