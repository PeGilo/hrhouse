﻿CREATE FUNCTION [dbo].[F_END_OF_DAY]
( @DAY datetime )
RETURNS datetime
AS
/*
Function: F_END_OF_DAY
	Finds start of next day
	for input datetime, @DAY.
	Valid for all SQL Server datetimes
*/

BEGIN
	RETURN dateadd(day, 1, dbo.F_START_OF_DAY(@DAY)) 
END