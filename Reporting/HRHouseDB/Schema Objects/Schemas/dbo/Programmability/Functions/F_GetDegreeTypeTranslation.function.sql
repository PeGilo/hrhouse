﻿CREATE FUNCTION [dbo].[F_GetDegreeTypeTranslation] 
(@Language nvarchar(5))
RETURNS @trans TABLE  (DETY_ID	INT, LOCALIZED_NAME	NVARCHAR(MAX))
AS
BEGIN

	IF (@Language IS NULL)  SELECT @Language = 'en-US'
		
	Declare @PreferedLanguageId INT
	select @PreferedLanguageId = LANGUAGE_ID 
	from [$(RecruitmentDatabaseName)].dbo.REP_LANGUAGE where [LANGUAGE_LOCALE_LANGUAGE] = SUBSTRING(@Language, 1, 2)
	
	Declare @DefaultLanguageId INT
	select @DefaultLanguageId = LANGUAGE_ID 
	from [$(RecruitmentDatabaseName)].dbo.REP_LANGUAGE where [LANGUAGE_LOCALE_LANGUAGE] = SUBSTRING('fi-FI', 1, 2)
	
	INSERT  @trans
	select DT.DETY_ID as 'DETY_ID', 
			COALESCE(				-- Selects first non-null value
				DT_LANG.DETL_TEXT,  -- First select prefered language
				DT_LANG2.DETL_TEXT, -- In case of no translate, select default language
				DT.DETY_NAME        -- In case of no language text select Name field
			) AS 'LOCALIZED_NAME'
	from [$(RecruitmentDatabaseName)].dbo.REP_DETY_DEGREE_TYPE as DT
	left outer join [$(RecruitmentDatabaseName)].dbo.REP_DETL_DEGREE_TYPE_LANG as DT_LANG
		ON DT_LANG.DETL_DEGREE_TYPE_ID = DT.DETY_ID AND DT_LANG.DETL_LANG_ID = @PreferedLanguageId
	left outer join [$(RecruitmentDatabaseName)].dbo.REP_DETL_DEGREE_TYPE_LANG as DT_LANG2 
		ON DT_LANG2.DETL_DEGREE_TYPE_ID = DT.DETY_ID AND DT_LANG2.DETL_LANG_ID = @DefaultLanguageId

	RETURN
END
