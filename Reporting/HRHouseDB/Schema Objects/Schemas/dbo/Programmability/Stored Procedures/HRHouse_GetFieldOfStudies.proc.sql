﻿CREATE PROCEDURE [dbo].[HRHouse_GetFieldOfStudies]
@Language NVARCHAR (5)
AS
BEGIN
	SET NOCOUNT ON;

If @Language = 'fi-FI'
	Begin
		SELECT -1 as Id, 'Ei Filtteriä' As Name
		UNION
		SELECT Id, Name
		FROM OPENQUERY(HRHOUSE_LINK, 'select liob_id as Id, liob_name as Name from liob_line_of_business') AS FieldOfStudies
		ORDER BY Id
	End
Else If @Language = 'en-US'
	Begin
		SELECT -1 as Id, 'No Filtering' As Name
		UNION
		SELECT Id, Name
		FROM OPENQUERY(HRHOUSE_LINK, 'select liob_id as Id, liob_name as Name from liob_line_of_business') AS FieldOfStudies
		ORDER BY Id
	End
END


