﻿CREATE PROCEDURE [dbo].[HRHouse_ApplicationProfile]
(
	@BeginDate	DATETIME, 
	@EndDate	DATETIME,
	@Language	NVARCHAR(5)
)	
AS
BEGIN

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	IF (@BeginDate IS NULL)  SELECT @BeginDate = dbo.DateTimeMinValue()
	IF (@EndDate IS NULL)  SELECT @EndDate = dbo.DateTimeMaxValue()
	IF (@Language IS NULL)  SELECT @Language = 'en-US'
	
	CREATE TABLE #temp
	(
		FilterNumber	INT,
		ParameterId		INT,
		ParameterName   NVARCHAR(MAX),
		ParameterOrderBy INT,
		Cts				DATETIME,
		Value			INT,
		[Rank]			INT,
		GroupName		NVARCHAR(MAX) DEFAULT NULL
	)
	
	INSERT INTO #temp EXEC [dbo].[HRHouse_ApplicationProfile_BasicEducation] @BeginDate, @EndDate, @Language
	INSERT INTO #temp EXEC [dbo].[HRHouse_ApplicationProfile_FurtherEducation] @BeginDate, @EndDate, @Language
	INSERT INTO #temp EXEC [dbo].[HRHouse_ApplicationProfile_EducationSectors] @BeginDate, @EndDate, @Language
	INSERT INTO #temp EXEC [dbo].[HRHouse_ApplicationProfile_EducationLevels] @BeginDate, @EndDate, @Language
	INSERT INTO #temp EXEC [dbo].[HRHouse_ApplicationProfile_CurrentSituation] @BeginDate, @EndDate, @Language		
	INSERT INTO #temp EXEC [dbo].[HRHouse_ApplicationProfile_IISExperience] @BeginDate, @EndDate, @Language	
	INSERT INTO #temp EXEC [dbo].[HRHouse_ApplicationProfile_WorkExperience] @BeginDate, @EndDate, @Language	
	INSERT INTO #temp EXEC [dbo].[HRHouse_ApplicationProfile_Qualification] @BeginDate, @EndDate, @Language	
	INSERT INTO #temp EXEC [dbo].[HRHouse_ApplicationProfile_FinnishLanguage] @BeginDate, @EndDate, @Language	
	INSERT INTO #temp EXEC [dbo].[HRHouse_ApplicationProfile_Internet] @BeginDate, @EndDate, @Language	
	INSERT INTO #temp EXEC [dbo].[HRHouse_ApplicationProfile_Email] @BeginDate, @EndDate, @Language	
	INSERT INTO #temp EXEC [dbo].[HRHouse_ApplicationProfile_Skills] @BeginDate, @EndDate, @Language	
	INSERT INTO #temp EXEC [dbo].[HRHouse_ApplicationProfile_Interests] @BeginDate, @EndDate, @Language	
	INSERT INTO #temp EXEC [dbo].[HRHouse_ApplicationProfile_Relationship] @BeginDate, @EndDate, @Language	
	INSERT INTO #temp EXEC [dbo].[HRHouse_ApplicationProfile_JobTypes] @BeginDate, @EndDate, @Language	
	INSERT INTO #temp EXEC [dbo].[HRHouse_ApplicationProfile_SuitableShifts] @BeginDate, @EndDate, @Language	
	INSERT INTO #temp EXEC [dbo].[HRHouse_ApplicationProfile_Nationality] @BeginDate, @EndDate, @Language	
	INSERT INTO #temp EXEC [dbo].[HRHouse_ApplicationProfile_AfterSalesPermission] @BeginDate, @EndDate, @Language	
	INSERT INTO #temp EXEC [dbo].[HRHouse_ApplicationProfile_Gender] @BeginDate, @EndDate, @Language	
	INSERT INTO #temp EXEC [dbo].[HRHouse_ApplicationProfile_InfoCanBeUsed] @BeginDate, @EndDate, @Language	
	INSERT INTO #temp EXEC [dbo].[HRHouse_ApplicationProfile_WhereFoundInfo] @BeginDate, @EndDate, @Language	
			
	select * from #temp
	
	drop table #temp	
END