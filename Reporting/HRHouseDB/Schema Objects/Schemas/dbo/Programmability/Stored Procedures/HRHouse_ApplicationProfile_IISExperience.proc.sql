﻿CREATE PROCEDURE [dbo].[HRHouse_ApplicationProfile_IISExperience]
(
	@BeginDate	DATETIME, 
	@EndDate	DATETIME,
	@Language	NVARCHAR(5)
)	
AS
BEGIN

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	IF (@BeginDate IS NULL)  SELECT @BeginDate = dbo.DateTimeMinValue()
	IF (@EndDate IS NULL)  SELECT @EndDate = dbo.DateTimeMaxValue()
	IF (@Language IS NULL)  SELECT @Language = 'en-US'

	-- FilterNumber, ParameterId, ParameterName, ParameterOrderBy, Cts, Value, Rank, GroupName
	SELECT	6,
			1, -- 'Worked here before'
			'WorkedHereBefore',
			1,
			HAKEMUS.cts,
			1,
			0, -- No ranks
			null
	FROM [$(RecruitmentDatabaseName)].dbo.REP_REKRY_HAKEMUS AS HAKEMUS
	WHERE HAKEMUS.CTS BETWEEN @BeginDate AND @EndDate
		  AND HAKEMUS.WORKED_HERE_BEFORE = 1
	UNION ALL
	SELECT	6,
			2, -- 'Internal applicant'
			'InternalApplicant',
			2,
			HAKEMUS.cts,
			1,
			0, -- No ranks
			null
	FROM [$(RecruitmentDatabaseName)].dbo.REP_REKRY_HAKEMUS AS HAKEMUS
	WHERE HAKEMUS.CTS BETWEEN @BeginDate AND @EndDate
		  AND HAKEMUS.INTERNAL_APPLICANT = 1
	ORDER BY HAKEMUS.CTS
		
END