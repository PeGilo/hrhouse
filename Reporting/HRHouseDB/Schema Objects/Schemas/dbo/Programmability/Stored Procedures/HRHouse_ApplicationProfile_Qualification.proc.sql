﻿CREATE PROCEDURE [dbo].[HRHouse_ApplicationProfile_Qualification]
(
	@BeginDate	DATETIME, 
	@EndDate	DATETIME,
	@Language	NVARCHAR(5)
)	
AS
BEGIN

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	IF (@BeginDate IS NULL)  SELECT @BeginDate = dbo.DateTimeMinValue()
	IF (@EndDate IS NULL)  SELECT @EndDate = dbo.DateTimeMaxValue()
	IF (@Language IS NULL)  SELECT @Language = 'en-US'
	

	-- FilterNumber, ParameterId, ParameterName, ParameterOrderBy, Cts, Value, Rank, GroupName
	select  8,
			TAITOS.[TAITO_ID], 
			TAITOS.[TEXT],
			0, -- No order by field
			HAK.CTS as [CTS],
			1,
			RANKING.[rank],
			TAITOS.GROUP_TEXT
	FROM (
			SELECT HAKEMUS.ID, TAITO.ID AS [TAITO_ID], TAITO_LANG.TAITO_TEXT AS [TEXT], RYHMA_LANG.TAITORYHMA_TEXT AS [GROUP_TEXT]
			FROM [$(RecruitmentDatabaseName)].dbo.REP_REKRY_HAKEMUS as HAKEMUS
			JOIN [$(RecruitmentDatabaseName)].dbo.REP_REKRY_HAKEMUKSEN_TAIDOT as HAK_TAIDOT ON HAKEMUS.ID = HAK_TAIDOT.HAKEMUS_ID
			JOIN [$(RecruitmentDatabaseName)].dbo.REP_REKRY_TAITO as TAITO ON HAK_TAIDOT.TAITO_ID = TAITO.ID 
			JOIN [$(RecruitmentDatabaseName)].dbo.REP_TAITO_LANG as TAITO_LANG ON TAITO.ID = TAITO_LANG.TAITO_ID AND TAITO_LANG.LANG_ID = 1
			JOIN [$(RecruitmentDatabaseName)].dbo.REP_REKRY_TAITORYHMA as RYHMA ON TAITO.TAITORYHMA_ID = RYHMA.ID
			JOIN [$(RecruitmentDatabaseName)].dbo.REP_TAITORYHMA_LANG AS RYHMA_LANG ON RYHMA_LANG.TAITORYHMA_ID = RYHMA.ID AND RYHMA_LANG.LANG_ID = 1
			WHERE RYHMA.TAITORYHMA_NIMI = N'Pätevyydet, luvat ja muu ammatillinen lisäkoulutus'
				  -- AND RYHMA.ID = 104			
				  AND HAKEMUS.CTS BETWEEN @BeginDate AND @EndDate
		  ) TAITOS
	-- As we should get CTS from HAKEMUS outside distinct, we have to join it.
	JOIN [$(RecruitmentDatabaseName)].dbo.REP_REKRY_HAKEMUS as HAK ON TAITOS.ID = HAK.ID		  
	-- Ranking result set, so every Parameter has its rank by Count
	JOIN (		
		-- As rank is used row number in sorted table
		SELECT row_number() over( ORDER BY COUNT([TAITO_ID]) desc) as [Rank], [TAITO_ID] 
		FROM (
			SELECT DISTINCT HAKEMUS.ID, TAITO.ID as [TAITO_ID]
			FROM [$(RecruitmentDatabaseName)].dbo.REP_REKRY_HAKEMUS as HAKEMUS
			JOIN [$(RecruitmentDatabaseName)].dbo.REP_REKRY_HAKEMUKSEN_TAIDOT as HAK_TAIDOT ON HAKEMUS.ID = HAK_TAIDOT.HAKEMUS_ID
			JOIN [$(RecruitmentDatabaseName)].dbo.REP_REKRY_TAITO as TAITO ON HAK_TAIDOT.TAITO_ID = TAITO.ID 
			JOIN [$(RecruitmentDatabaseName)].dbo.REP_REKRY_TAITORYHMA as RYHMA ON TAITO.TAITORYHMA_ID = RYHMA.ID
			JOIN [$(RecruitmentDatabaseName)].dbo.REP_TAITO_LANG as TAITO_LANG ON TAITO.ID = TAITO_LANG.TAITO_ID AND TAITO_LANG.LANG_ID = 1
			WHERE RYHMA.TAITORYHMA_NIMI = N'Pätevyydet, luvat ja muu ammatillinen lisäkoulutus'
				-- AND RYHMA.ID = 104	
				  AND HAKEMUS.CTS BETWEEN @BeginDate AND @EndDate
		) T
		GROUP BY [TAITO_ID]
		) RANKING ON TAITOS.[TAITO_ID] = RANKING.[TAITO_ID]
	ORDER BY [CTS]		
END
