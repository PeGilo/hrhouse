﻿CREATE PROCEDURE [dbo].[HRHouse_ReportStyle]
	@ReportStyleId INT
AS
BEGIN

	DECLARE @columns NVARCHAR(MAX);

	SELECT @columns = STUFF(
	(
		SELECT ', ' + QUOTENAME(ElementName, '[') AS [text()]
		FROM
			(
		  SELECT e.[ElementName] , ISNULL(ElementValue,ElementDefaultValue) AS Value
			FROM [Element] e WITH (NOLOCK)
			LEFT JOIN [ReportStyleElements] rse WITH (NOLOCK) ON
			rse.[ElementId] = e.[ElementId]
			AND rse.[ReportStyleId] = @ReportStyleId) ReportStyleElements
			FOR XML PATH ('')
			)
	, 1, 1, '');
	                       
	DECLARE @sql NVARCHAR(MAX);
	SET @sql = 'SELECT ' + @columns + '
	INTO temp
	FROM (
	SELECT e.[ElementName] , ISNULL(ElementValue,ElementDefaultValue) AS Value	
	FROM  [Element] e WITH (NOLOCK)
	LEFT JOIN [ReportStyleElements] rse WITH (NOLOCK) ON
	rse.[ElementId] = e.[ElementId] AND rse.[ReportStyleId] = ' + CAST(@ReportStyleId AS VARCHAR (9)) + '
	 
	) AS ReportStyleElements
	PIVOT ( MIN(Value) FOR ElementName IN (' + @columns + ')) AS [Elements]';

	EXEC sp_ExecuteSQL @sql
	
	SELECT * FROM temp
	DROP TABLE temp
END