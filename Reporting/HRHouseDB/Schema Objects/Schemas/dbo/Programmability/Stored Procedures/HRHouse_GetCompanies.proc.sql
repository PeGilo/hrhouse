﻿CREATE PROCEDURE [dbo].[HRHouse_GetCompanies]
@Language NVARCHAR (5)
AS
BEGIN
	SET NOCOUNT ON;

If @Language = 'fi-FI'
	Begin
		SELECT -1 as Id, 'Ei Filtteriä' As Name
		UNION
		SELECT Id, Name
		FROM OPENQUERY(HRHOUSE_LINK, 'select grou_id as Id, grou_name as Name from grou_grouping') AS Companies
		ORDER BY Id
	End
Else If @Language = 'en-US'
	Begin
		SELECT -1 as Id, 'No Filtering' As Name
		UNION
		SELECT Id, Name
		FROM OPENQUERY(HRHOUSE_LINK, 'select grou_id as Id, grou_name as Name from grou_grouping') AS Companies
		ORDER BY Id
	End
	
END


