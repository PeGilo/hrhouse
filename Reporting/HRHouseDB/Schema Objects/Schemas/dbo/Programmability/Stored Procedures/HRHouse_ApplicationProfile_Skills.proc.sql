﻿CREATE PROCEDURE [dbo].[HRHouse_ApplicationProfile_Skills]
(
	@BeginDate	DATETIME, 
	@EndDate	DATETIME,
	@Language	NVARCHAR(5)
)	
AS
BEGIN

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	IF (@BeginDate IS NULL)  SELECT @BeginDate = dbo.DateTimeMinValue()
	IF (@EndDate IS NULL)  SELECT @EndDate = dbo.DateTimeMaxValue()
	IF (@Language IS NULL)  SELECT @Language = 'en-US'
	
	-- FilterNumber, ParameterId, ParameterName, ParameterOrderBy, Cts, Value, Rank, GroupName
	SELECT  12,
			TAITO.ID,
			TAITO_LANG.TAITO_TEXT,
			0, -- No order by field
			HAKEMUS.CTS,
			1,
			0, -- No ranks
			RYHMA_LANG.TAITORYHMA_TEXT -- Group name
	
	FROM [$(RecruitmentDatabaseName)].dbo.REP_REKRY_HAKEMUS as HAKEMUS
	JOIN [$(RecruitmentDatabaseName)].dbo.REP_REKRY_HAKEMUKSEN_TAIDOT as HAK_TAIDOT ON HAKEMUS.ID = HAK_TAIDOT.HAKEMUS_ID
	JOIN [$(RecruitmentDatabaseName)].dbo.REP_REKRY_TAITO as TAITO ON HAK_TAIDOT.TAITO_ID = TAITO.ID 
	JOIN [$(RecruitmentDatabaseName)].dbo.REP_TAITO_LANG as TAITO_LANG ON TAITO.ID = TAITO_LANG.TAITO_ID AND TAITO_LANG.LANG_ID = 1
	JOIN [$(RecruitmentDatabaseName)].dbo.REP_REKRY_TAITORYHMA as RYHMA ON TAITO.TAITORYHMA_ID = RYHMA.ID
	JOIN [$(RecruitmentDatabaseName)].dbo.REP_TAITORYHMA_LANG AS RYHMA_LANG ON RYHMA.ID = RYHMA_LANG.TAITORYHMA_ID
	WHERE (RYHMA.TAITORYHMA_NIMI = N'new_skills_Toimisto-, asiantuntija- ja johtot_0000'
		OR 	RYHMA.TAITORYHMA_NIMI = N'new_skills_Siivous- ja ympäristöalan työnteki_0000'
		OR 	RYHMA.TAITORYHMA_NIMI = N'new_skills_Kiinteistönhoidon, rakentamisen ja_0000'
		OR 	RYHMA.TAITORYHMA_NIMI = N'new_skills_Majoitus- ja ravintola-alan työteh_0000'
		OR 	RYHMA.TAITORYHMA_NIMI = N'new_skills_Teollisuuden työtehtävät_0000'
		OR 	RYHMA.TAITORYHMA_NIMI = N'new_skills_Viheralan työtehtävät_0000'
		OR 	RYHMA.TAITORYHMA_NIMI = N'new_skills_Turvallisuusalan tehtävät_0000'
		OR 	RYHMA.TAITORYHMA_NIMI = N'new_skills_Ilmailualan työtehtävät_0000'
		OR 	RYHMA.TAITORYHMA_NIMI = N'new_skills_Logistiikka- ja kuljetusalan työte_0000'
		OR 	RYHMA.TAITORYHMA_NIMI = N'new_skills_Informaatioteknologian ja tietolii_0000'
		OR 	RYHMA.TAITORYHMA_NIMI = N'new_skills_Kaupan alan työtehtävät_0000'
		OR 	RYHMA.TAITORYHMA_NIMI = N'new_skills_Hoitoalan työtehtävät_0000'
		)
		 -- AND RYHMA.ID = 168 - 179		
		  
	AND HAKEMUS.CTS BETWEEN @BeginDate AND @EndDate
	ORDER BY HAKEMUS.CTS

	
END