﻿CREATE PROCEDURE [dbo].[HRHouse_ApplicationProfile_WorkExperience]
(
	@BeginDate	DATETIME, 
	@EndDate	DATETIME,
	@Language	NVARCHAR(5)
)	
AS
BEGIN

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	IF (@BeginDate IS NULL)  SELECT @BeginDate = dbo.DateTimeMinValue()
	IF (@EndDate IS NULL)  SELECT @EndDate = dbo.DateTimeMaxValue()
	IF (@Language IS NULL)  SELECT @Language = 'en-US'
	
	SELECT [PRFI_ID], [LOCALIZED_NAME] INTO #ProfessionFieldLocalized FROM dbo.[F_GetProfessionFieldTranslation](@Language) 
	

	-- FilterNumber, ParameterId, ParameterName, ParameterOrderBy, Cts, Value, Rank, GroupName
	SELECT  7,
			DISTINCT_PROFFIELDS.PRFI_ID,
			PROF_LOCAL.LOCALIZED_NAME,
			DISTINCT_PROFFIELDS.PRFI_ORDER_BY,
			HAK.cts as [CTS],
			1 as [Value],
			RANKING.[Rank],
			null
	FROM ( -- We want select only non-repeating Profession Fields for Applications
			SELECT DISTINCT HAKEMUS.ID, PROF.PRFI_ID, PROF.PRFI_ORDER_BY
			FROM [$(RecruitmentDatabaseName)].dbo.REP_REKRY_HAKEMUS as HAKEMUS
			JOIN [$(RecruitmentDatabaseName)].dbo.REP_WORK_EXPERIENCE as WE ON HAKEMUS.ID = WE.WE_APPLICATION_ID
			JOIN [$(RecruitmentDatabaseName)].dbo.REP_PRFI_PROFESSION_FIELD AS PROF ON WE.WE_PROFESSION_FIELD = PROF.PRFI_ID
			
			WHERE HAKEMUS.CTS BETWEEN @BeginDate AND @EndDate
		) AS DISTINCT_PROFFIELDS
	-- Geting localization for degrees
	LEFT OUTER JOIN #ProfessionFieldLocalized AS PROF_LOCAL ON DISTINCT_PROFFIELDS.PRFI_ID = PROF_LOCAL.PRFI_ID
	-- As we should get CTS from HAKEMUS outside distinct, we have to join it.
	JOIN [$(RecruitmentDatabaseName)].dbo.REP_REKRY_HAKEMUS as HAK ON DISTINCT_PROFFIELDS.ID = HAK.ID
	-- Ranking result set, so every Parameter has its rank by Count
	JOIN (		
		-- As rank is used row number in sorted table
		SELECT row_number() over( ORDER BY COUNT(PRFI_ID) desc) as [Rank], PRFI_ID 
		FROM (
			SELECT DISTINCT HAKEMUS.ID, PROF.PRFI_ID
			FROM [$(RecruitmentDatabaseName)].dbo.REP_REKRY_HAKEMUS as HAKEMUS
			JOIN [$(RecruitmentDatabaseName)].dbo.REP_WORK_EXPERIENCE as WE ON HAKEMUS.ID = WE.WE_APPLICATION_ID
			JOIN [$(RecruitmentDatabaseName)].dbo.REP_PRFI_PROFESSION_FIELD AS PROF ON WE.WE_PROFESSION_FIELD = PROF.PRFI_ID
			WHERE HAKEMUS.CTS BETWEEN @BeginDate AND @EndDate
		) T
		GROUP BY PRFI_ID
		) RANKING ON DISTINCT_PROFFIELDS.PRFI_ID = RANKING.PRFI_ID
	UNION ALL

	-- Unite with 'I_dont_have_we' information	
	-- FilterNumber, ParameterId, ParameterName, ParameterOrderBy, Cts, Value, Rank
	select  7,
			0, -- Must not match with any PRFI_ID
			'i_dont_have_we', -- Needed to be translated in report
			0, -- Should be positioned first
			HAKEMUS.CTS as [CTS],
			1,
			0,  -- Should get into table anyway, rank does not make difference
			null
	FROM [$(RecruitmentDatabaseName)].dbo.REP_REKRY_HAKEMUS as HAKEMUS
	JOIN [$(RecruitmentDatabaseName)].dbo.REP_REKRY_HAKEMUKSEN_TAIDOT as HAK_TAIDOT ON HAKEMUS.ID = HAK_TAIDOT.HAKEMUS_ID
	JOIN [$(RecruitmentDatabaseName)].dbo.REP_REKRY_TAITO as TAITO ON HAK_TAIDOT.TAITO_ID = TAITO.ID 
	WHERE TAITO.TAITO_NIMI = 'i_dont_have_we'
		  AND HAKEMUS.CTS BETWEEN @BeginDate AND @EndDate
	ORDER BY [CTS]

END