﻿CREATE PROCEDURE [dbo].[HRHouse_ApplicationProfile_Relationship]
(
	@BeginDate	DATETIME, 
	@EndDate	DATETIME,
	@Language	NVARCHAR(5)
)	
AS
BEGIN

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	IF (@BeginDate IS NULL)  SELECT @BeginDate = dbo.DateTimeMinValue()
	IF (@EndDate IS NULL)  SELECT @EndDate = dbo.DateTimeMaxValue()
	IF (@Language IS NULL)  SELECT @Language = 'en-US'

	-- FilterNumber, ParameterId, ParameterName, ParameterOrderBy, Cts, Value, Rank, GroupName
	SELECT	14,
			1, -- 'Full time job'
			'FullTimeJob',
			1,
			HAKEMUS.cts,
			1,
			0, -- No ranks
			null
	FROM [$(RecruitmentDatabaseName)].dbo.REP_REKRY_HAKEMUS AS HAKEMUS
	WHERE HAKEMUS.CTS BETWEEN @BeginDate AND @EndDate
		  AND HAKEMUS.FULL_TIME_JOB = 1
	UNION ALL
	SELECT	14,
			2, -- 'Part time'
			'PartTime',
			2,
			HAKEMUS.cts,
			1,
			0, -- No ranks
			null
	FROM [$(RecruitmentDatabaseName)].dbo.REP_REKRY_HAKEMUS AS HAKEMUS
	WHERE HAKEMUS.CTS BETWEEN @BeginDate AND @EndDate
		  AND HAKEMUS.PART_TIME = 1
	ORDER BY HAKEMUS.CTS
		
END