﻿CREATE PROCEDURE [dbo].[HR_House_GetGroups]
@Language NVARCHAR (5)
AS
BEGIN	
	
	CREATE TABLE #tmpGroups
	(
		[ID] varchar(8),
		[Text] varchar(100) 
	)
	
	IF @Language = 'fi-FI'
	BEGIN
	
		INSERT INTO #tmpGroups ([ID], [Text])
		SELECT '-1' as [ID], 'Ei Filtteriä' As [Text]
		
	END
	ELSE 
	BEGIN
	
		INSERT INTO #tmpGroups ([ID], [Text])
		SELECT '-1' as [ID], 'No Filtering' As [Text]
		
	END	
	
	INSERT INTO #tmpGroups ([ID], [Text])
	SELECT DISTINCT c.customerpin as [ID], c.RagioneSociale as [Text] 
	FROM OrderManagement.dbo.joinAdmGroupInvoice ag
		INNER JOIN OrderManagement.dbo.customer c ON c.customerpin = ag.GroupPin
	ORDER BY 2	
	
	SELECT * FROM #tmpGroups
	
	DROP TABLE #tmpGroups
END