﻿CREATE PROCEDURE [dbo].[HRHouse_GetEmploymentRelationships]
@Language NVARCHAR (5)
AS
BEGIN
	SET NOCOUNT ON;

If @Language = 'fi-FI'
	Begin
		SELECT -1 as Id, 'Ei Filtteriä' As Name
		UNION
		SELECT Id, Name
		FROM OPENQUERY(HRHOUSE_LINK, 'SELECT
			jode_id as Id,
			concat_ws('' - '',joty_name,jode_name) as Name
		FROM
			mpy.joty_job_detail_type,
			mpy.jode_job_detail
		WHERE 
			joty_id = jode_job_detail_type_id') AS EmploymentRelationships
		ORDER BY Id
	End
Else If @Language = 'en-US'
	Begin
		SELECT -1 as Id, 'No Filtering' As Name
		UNION
		SELECT Id, Name
		FROM OPENQUERY(HRHOUSE_LINK, 'SELECT
			jode_id as Id,
			concat_ws('' - '',joty_name,jode_name) as Name
		FROM
			mpy.joty_job_detail_type,
			mpy.jode_job_detail
		WHERE 
			joty_id = jode_job_detail_type_id') AS EmploymentRelationships
		ORDER BY Id
	End	
END


