﻿CREATE PROCEDURE [dbo].[HRHouse_ApplicationProfile_InfoCanBeUsed]
(
	@BeginDate	DATETIME, 
	@EndDate	DATETIME,
	@Language	NVARCHAR(5)
)	
AS
BEGIN

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	IF (@BeginDate IS NULL)  SELECT @BeginDate = dbo.DateTimeMinValue()
	IF (@EndDate IS NULL)  SELECT @EndDate = dbo.DateTimeMaxValue()
	IF (@Language IS NULL)  SELECT @Language = 'en-US'

	-- FilterNumber, ParameterId, ParameterName, ParameterOrderBy, Cts, Value, Rank, GroupName
	SELECT	20,
			1,	-- ParameterId is one, so that can be any value
			'', -- Name is not shown, so empty
			0,
			HAKEMUS.CTS,
			HAKEMUS.MY_INFO_CAN_BE_USED,
			0,	-- No ranks
			null
	FROM [$(RecruitmentDatabaseName)].dbo.REP_REKRY_HAKEMUS AS HAKEMUS
	WHERE HAKEMUS.CTS BETWEEN @BeginDate AND @EndDate
	ORDER BY HAKEMUS.CTS
		
END