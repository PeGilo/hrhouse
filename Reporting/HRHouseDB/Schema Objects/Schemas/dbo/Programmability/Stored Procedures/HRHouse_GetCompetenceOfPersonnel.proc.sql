﻿CREATE PROCEDURE [dbo].[HRHouse_GetCompetenceOfPersonnel]
@Language NVARCHAR (5)
AS
BEGIN
	SET NOCOUNT ON;

IF @Language = 'fi-FI'
	BEGIN
		--SELECT -1 as Id, 'Ei Filtteriä' as Name
		--UNION
		SELECT Id, Name
		FROM OPENQUERY(HRHOUSE_LINK, 'SELECT ciat_id as Id, syte_fi as Name 
			FROM
				mpy.ciat_competence_inquiry_answer_type,
				mpy.syte_system_text
			WHERE
				syte_key = Right(ciat_name, Length(ciat_name) - 1) And
				Left(ciat_name,1) = ''!''  
			UNION
			SELECT ciat_id as Id, ciat_name as Name
			FROM
				mpy.ciat_competence_inquiry_answer_type
			WHERE
				Left(ciat_name,1) <> ''!''') AS CompetenceOfPersonnel
		ORDER BY Id
	END
ELSE IF @Language = 'en-US'  
	BEGIN
		--SELECT -1 as Id, 'No Filtering' as Name
		--UNION
		SELECT Id, Name
		FROM OPENQUERY(HRHOUSE_LINK, 'SELECT ciat_id as Id, syte_en as Name 
			FROM
				mpy.ciat_competence_inquiry_answer_type,
				mpy.syte_system_text
			WHERE
				syte_key = Right(ciat_name, Length(ciat_name) - 1) And
				Left(ciat_name,1) = ''!''  
			UNION
			SELECT ciat_id as Id, ciat_name as Name
			FROM
				mpy.ciat_competence_inquiry_answer_type
			WHERE
				Left(ciat_name,1) <> ''!''') AS CompetenceOfPersonnel
		ORDER BY Id
	END
END


