﻿CREATE  PROCEDURE [dbo].[HRHouse_Turnover]
	@ReportPeriodType TINYINT, 
	@ReportDateFrom DATETIME, 
	@ReportDateTo DATETIME,
	@Scope NVARCHAR (MAX), 
	@CustomerId VARCHAR (8) = NULL, 
	@GroupId VARCHAR (8) = NULL, 
	@SuperGroupId INT = NULL, 
	@OnlyActiveCustomer BIT = 0
AS
BEGIN
	
	IF @CustomerId = '-1' SET @CustomerId = NULL
	IF @GroupId = '-1' SET @GroupId = NULL
	IF @SuperGroupId = -1 SET @SuperGroupId = NULL;
	
	
	SELECT  		 
		c1.customerpin			AS [CustomerID],
		sg.RagioneSociale		AS [SuperGroup], 
		c.RagioneSociale		AS [Group], 		
		c1.RagioneSociale		AS [CostCenter],
		(
			SELECT    SUM(abs((ii.sumofcost)) + abs(ISNULL(ii.ConfLater, 0)) + abs(ISNULL(ii.RapidoPreno, 0)) + abs(ISNULL(ii.[VV-lisa], 0))
					  + abs(ISNULL(ii.travelMoney, 0)))
			FROM      [$(OrderManagementDatabaseName)].dbo.invoices i INNER JOIN
					  [$(OrderManagementDatabaseName)].dbo.invoiceitems ii ON i.invoicenum = ii.invoicenum AND i.customerpin = ii.customerpin AND i.anno = ii.anno 
			WHERE     (i.dateend >= r.Begindate) AND (i.dateend < r.Enddate) AND (pl.customerChild = ii.locationPin)
		)						AS [Turnover],
		r.Begindate				AS [Range] 
	FROM	[$(OrderManagementDatabaseName)].dbo.joinSuperGroup sg 
			RIGHT OUTER JOIN [$(OrderManagementDatabaseName)].dbo.joinAdmGroupInvoice ag ON sg.id = ag.SuperGroupId			
			INNER JOIN [$(OrderManagementDatabaseName)].dbo.joinParentLocation  pl ON ag.GroupPin = pl.customerPin
			INNER JOIN [$(OrderManagementDatabaseName)].dbo.customer c ON c.customerpin = ag.GroupPin 
			INNER JOIN [$(OrderManagementDatabaseName)].dbo.customer AS c1 ON c1.customerpin = pl.customerChild
			CROSS APPLY dbo.F_GetDateRanges(@ReportDateFrom, @ReportDateTo, @ReportPeriodType, @Scope) r
	WHERE c1.customerpin = ISNULL(@CustomerId, c1.customerpin) AND
		  c.customerpin = ISNULL(@GroupId, c.customerpin) AND
		  (@SuperGroupId is NULL OR sg.id = @SuperGroupId) AND
		  (@OnlyActiveCustomer = 0 OR c1.active = 1)		
	ORDER BY 2, 3, 4, 6
	OPTION (RECOMPILE)
	
END