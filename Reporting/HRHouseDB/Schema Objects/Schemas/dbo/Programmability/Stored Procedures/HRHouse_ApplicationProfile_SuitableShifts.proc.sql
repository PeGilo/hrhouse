﻿CREATE PROCEDURE [dbo].[HRHouse_ApplicationProfile_SuitableShifts]
(
	@BeginDate	DATETIME, 
	@EndDate	DATETIME,
	@Language	NVARCHAR(5)
)	
AS
BEGIN

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	IF (@BeginDate IS NULL)  SELECT @BeginDate = dbo.DateTimeMinValue()
	IF (@EndDate IS NULL)  SELECT @EndDate = dbo.DateTimeMaxValue()
	IF (@Language IS NULL)  SELECT @Language = 'en-US'

	-- FilterNumber, ParameterId, ParameterName, ParameterOrderBy, Cts, Value, Rank, GroupName
	select  16,
			DISTINCT_SHIFTS.WS_ID,
			DISTINCT_SHIFTS.WS_NAME,
			DISTINCT_SHIFTS.WS_ORDER_BY, 
			HAK.CTS,
			1,
			0, -- No ranks
			null	
	FROM
	(
	SELECT DISTINCT HAKEMUS.ID AS [HAKEMUS_ID],
			SHIFT.WS_ID,
			SHIFT.WS_NAME,
			SHIFT.WS_ORDER_BY
	FROM [$(RecruitmentDatabaseName)].dbo.REP_REKRY_HAKEMUS AS HAKEMUS
	JOIN [$(RecruitmentDatabaseName)].dbo.REP_APPLICATION_WORK_SHIFT AS APP_SHIFT ON APP_SHIFT.AWS_APP_ID = HAKEMUS.ID
	JOIN [$(RecruitmentDatabaseName)].dbo.REP_WORK_SHIFT AS SHIFT ON SHIFT.WS_ID = APP_SHIFT.AWS_SHIFT_ID
	WHERE HAKEMUS.CTS BETWEEN @BeginDate AND @EndDate
	) DISTINCT_SHIFTS
	-- As we should get CTS from HAKEMUS outside distinct, we have to join it.
	JOIN [$(RecruitmentDatabaseName)].dbo.REP_REKRY_HAKEMUS as HAK ON DISTINCT_SHIFTS.[HAKEMUS_ID] = HAK.ID
	ORDER BY HAK.CTS
		
END