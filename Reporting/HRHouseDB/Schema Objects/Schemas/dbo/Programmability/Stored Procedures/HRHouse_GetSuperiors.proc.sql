﻿CREATE PROCEDURE [dbo].[HRHouse_GetSuperiors]
@Language NVARCHAR (5)
AS
BEGIN
	SET NOCOUNT ON;

If @Language = 'fi-FI'
	Begin
		SELECT -1 as Id, 'Ei Filtteriä' As Name
		UNION
		SELECT Id, Name
		FROM OPENQUERY(HRHOUSE_LINK, 'SELECT 
			pede_personal_detail.pede_id as Id,
		    pede_personal_detail.pede_first_name as Name
		FROM
			mpy.pede_personal_detail,
			mpy.emrp_employment_relationship_person
		WHERE
			pede_personal_detail.pede_id = emrp_employment_relationship_person.emrp_personal_detail_id AND
			emrp_employment_relationship_person_type_id = 20') AS Superiors
		ORDER BY Id
	End
Else If @Language = 'en-US'
	Begin
		SELECT -1 as Id, 'No Filtering' As Name
		UNION
		SELECT Id, Name
		FROM OPENQUERY(HRHOUSE_LINK, 'SELECT 
			pede_personal_detail.pede_id as Id,
		    pede_personal_detail.pede_first_name as Name
		FROM
			mpy.pede_personal_detail,
			mpy.emrp_employment_relationship_person
		WHERE
			pede_personal_detail.pede_id = emrp_employment_relationship_person.emrp_personal_detail_id AND
			emrp_employment_relationship_person_type_id = 20') AS Superiors
		ORDER BY Id
	End
				
END


