﻿CREATE PROCEDURE [dbo].[HR_House_GetCustomers]
@Language NVARCHAR (5)
AS
BEGIN

	CREATE TABLE #tmpCustomers
	(
		[ID] varchar(8),
		[Text] varchar(100) 
	)

	IF @Language = 'fi-FI'
	BEGIN
	
		INSERT INTO #tmpCustomers ([ID], [Text])
		SELECT '-1' as [ID], 'Ei Filtteriä' As [Text]
		
	END
	ELSE 
	BEGIN
	
		INSERT INTO #tmpCustomers ([ID], [Text])
		SELECT '-1' as [ID], 'No Filtering' As [Text]
		
	END
	
	INSERT INTO #tmpCustomers ([ID], [Text])
	SELECT c. customerpin as [ID], c.RagioneSociale as [Text] 
	FROM OrderManagement.dbo.customer c 
	ORDER BY 2
	
	SELECT * FROM #tmpCustomers
	
	DROP TABLE #tmpCustomers
	
END