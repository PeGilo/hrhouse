﻿CREATE PROCEDURE [dbo].[HRHouse_Recruitment]
@BeginDate DATETIME, @EndDate DATETIME
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	IF (@BeginDate IS NULL)  SELECT @BeginDate = dbo.DateTimeMinValue()
	IF (@EndDate IS NULL)  SELECT @EndDate = dbo.DateTimeMaxValue()
	
    SELECT 
		j.TYO_TYON_NIMI as 'Tehtävä',						-- Job name
		j.VISIBLE_IN_HOMEPAGE_END as 'Hakuaika päättyy',	-- Application deadline
		j.ORDER_N_OF_UNITS as 'Henkilömäärä',				-- amount of people to be recruited
		j.NEW_JOB as 'Uusi paikka',							-- new job
		(select k.KAYTTAJA_ETUNIMI + ' ' + k.KAYTTAJA_SUKUNIMI 
		from [$(RecruitmentDatabaseName)].dbo.REP_REKRY_KAYTTAJA k
		where k.KAYTTAJA_ID = j.TYO_MANAGER_ID) as 'Esimies', -- superior
		(select top(1) k.KAYTTAJA_ETUNIMI + ' ' + k.KAYTTAJA_SUKUNIMI
		from [$(RecruitmentDatabaseName)].dbo.REP_REKRY_KAYTTAJA k
			INNER JOIN [$(RecruitmentDatabaseName)].dbo.REP_JOB_ACCEPT ja ON ja.JA_USER_ID = k.KAYTTAJA_ID
		where j.TYO_ID = ja.JA_JOB_ID
		) as 'Vastuukonsultti',								-- responsible consultant
		p.PAIKKA_NIMI as 'Yritys',							-- Business area
		a.ALA_NIMI as 'Toimiala',							-- Organizational entity
		s.JOST_NAME as 'Hakustatus',						-- job status
		t.TJ_NAME as 'Työsuhdemuoto',						-- emploee form
		g.SG_NAME as 'Työntekijä/Toimih enkilö',			-- Staff group
		(select COUNT(1) 
		from [$(RecruitmentDatabaseName)].dbo.REP_REKRY_HAKEMUS_TYO h
		where j.TYO_ID = h.HAKEMUS_TYO_TYO_ID) as 'Hakemusten määrä',	-- amount of applications
		lo.JLO_NAME as 'Sijainti',							-- Region / City 
		(	case 
				when j.CHANNEL_OUTER = 1 AND j.CHANNEL_INNER = 1 then 'Ulkoinen/Sisäinen'
				when j.CHANNEL_OUTER = 1 AND j.CHANNEL_INNER = 0 then 'Ulkoinen'
				when j.CHANNEL_OUTER = 0 AND j.CHANNEL_INNER = 1 then 'Sisäinen'
				else ''
			end ) as 'Rekrytoinnin muoto',					-- Recruitment form
		j.CTS as 'Created'
    FROM [$(RecruitmentDatabaseName)].dbo.REP_REKRY_TYO j 		
		INNER JOIN [$(RecruitmentDatabaseName)].dbo.REP_REKRY_PAIKKA p ON j.TYO_PAIKKA_ID = p.ID
		INNER JOIN [$(RecruitmentDatabaseName)].dbo.REP_REKRY_ALA a ON j.TYO_ALA_ID = a.ID
		INNER JOIN [$(RecruitmentDatabaseName)].dbo.REP_JOST_JOB_STATUS s ON j.TYO_STATUS_ID = s.JOST_ID
		INNER JOIN [$(RecruitmentDatabaseName)].dbo.REP_JOB_TYPE t ON j.TYO_TYPE_OF_JOB_ID = t.TJ_ID
		INNER JOIN [$(RecruitmentDatabaseName)].dbo.REP_STAFF_GROUP g ON j.TYO_STAFF_GROUP_ID = g.SG_ID
		INNER JOIN [$(RecruitmentDatabaseName)].dbo.REP_JOB_LOCATION l ON l.JL_JOB_ID = j.TYO_ID  
		INNER JOIN [$(RecruitmentDatabaseName)].dbo.REP_JOB_LOCATION_OPTION lo ON lo.JLO_ID = l.JL_LOCATION_ID 
	WHERE j.CTS BETWEEN @BeginDate AND @EndDate
	ORDER BY p.PAIKKA_NIMI, a.ALA_NIMI, ISNULL(j.VISIBLE_IN_HOMEPAGE_END, dbo.DateTimeMaxValue())
END