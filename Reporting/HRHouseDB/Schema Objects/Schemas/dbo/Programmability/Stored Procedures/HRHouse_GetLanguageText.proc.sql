﻿CREATE PROCEDURE dbo.HRHouse_GetLanguageText
	@Language nvarchar(5)
AS
BEGIN

	 SELECT [Id], [Key], [Language], [Text]
	 FROM dbo.LanguageText
	 WHERE [Language] = @Language
	  
END