﻿CREATE PROCEDURE [dbo].[HRHouse_ApplicationProfile_BasicEducation]
(
	@BeginDate	DATETIME, 
	@EndDate	DATETIME,
	@Language	NVARCHAR(5)
)	
AS
BEGIN

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	IF (@BeginDate IS NULL)  SELECT @BeginDate = dbo.DateTimeMinValue()
	IF (@EndDate IS NULL)  SELECT @EndDate = dbo.DateTimeMaxValue()
	IF (@Language IS NULL)  SELECT @Language = 'en-US'
	
	-- FilterNumber, ParameterId, ParameterName, ParameterOrderBy, Cts, Value, Rank, GroupName
	SELECT 1, HAKEMUS.KOULUTUS_ID, 
			  KOUL.KOULUTUS_NIMI,
			  KOUL.KOULUTUS_ORDERBY,
			  HAKEMUS.cts, 
			  1,
			  0,		-- No ranks
			  null
	
	from [$(RecruitmentDatabaseName)].dbo.REP_REKRY_HAKEMUS as HAKEMUS
	left outer join [$(RecruitmentDatabaseName)].dbo.REP_REKRY_KOULUTUS as KOUL ON HAKEMUS.KOULUTUS_ID = KOUL.ID
	WHERE HAKEMUS.CTS BETWEEN @BeginDate AND @EndDate
	order by HAKEMUS.CTS 
END