﻿CREATE PROCEDURE [dbo].[HRHouse_CompetenceSimulation]
(
	@Person1					NVARCHAR (MAX), 
	@Person2					NVARCHAR (MAX), 
	@Sex1						INT, 
	@Sex2						INT, 
	@Company1					INT, 
	@Company2					INT, 
	@BasicEducation1			INT, 
	@BasicEducation2			INT, 
	@EducationLevel1			INT, 
	@EducationLevel2			INT, 
	@FieldOfStudy1				INT, 
	@FieldOfStudy2				INT, 
	@EmploymentRelationship1	VARCHAR (MAX), 
	@EmploymentRelationship2	VARCHAR (MAX), 
	@Superior1					INT, 
	@Superior2					INT,
	@CompetenceOfPersonnel1		INT, 
	@CompetenceOfPersonnel2		INT, 
	@CompetenceGroup			NVARCHAR (MAX), 
	@CompetenceArea				NVARCHAR (MAX), 
	@Competence					NVARCHAR (MAX),
	@UseSecondGroup				bit
)
AS
BEGIN
	
	CREATE TABLE #CompetenceSimulationData
	(
		PersonnelGroup	TINYINT, 
		AnswerDate		DATE, 
		CompetenceValue NUMERIC (18), 
		CompetenceName	VARCHAR (MAX), 
		PersonName		VARCHAR (MAX),
		PersonId		INT, 
		CompetenceType	INT,
		PersonnelGroupCount INT
	)

	DECLARE @StartDate DATE, @EndDate DATE
	DECLARE @PersonnelGroupCount1		INT 
	DECLARE @PersonnelGroupCount2		INT 
	DECLARE @PersonnelGroupCondition1 nvarchar(max), @PersonnelGroupCondition2 nvarchar(max)
	DECLARE @SQLStringTemplate nvarchar(max)
	DECLARE @SQLString nvarchar(max)
	DECLARE @ParmDefinition NVARCHAR(500)
	
	SET @StartDate = dbo.DateTimeMinValue()
	SET @EndDate = dbo.DateTimeMaxValue()
	SET @ParmDefinition = N'@PersonnelGroupCount INT OUTPUT'
	Set @SQLStringTemplate ='SELECT COUNT(1) as PersonnelGroupCount
	FROM 
		mpy.pede_personal_detail
	WHERE TRUE'
			
	INSERT INTO #CompetenceSimulationData(AnswerDate, CompetenceValue, CompetenceName, PersonName, PersonId, CompetenceType)
	exec dbo.HRHouse_GetCompetenceSurveillanceData @Person1, @StartDate, @EndDate, @Sex1, @Company1, @BasicEducation1, @EducationLevel1, @FieldOfStudy1, @EmploymentRelationship1, @Superior1, @CompetenceOfPersonnel1, @CompetenceGroup, @CompetenceArea, @Competence, @PersonnelGroupCondition1 OUT
	
	SET @SQLString = @SQLStringTemplate + @PersonnelGroupCondition1	
	
	SET @SQLString = N'select @PersonnelGroupCount = PersonnelGroupCount from OPENQUERY(HRHOUSE_LINK, ''' +  REPLACE(@SQLString, '''', '''''') + ''')'
	
	EXEC sp_executesql @SQLString, @ParmDefinition, @PersonnelGroupCount = @PersonnelGroupCount1 OUTPUT	
		
	UPDATE #CompetenceSimulationData
	SET PersonnelGroup = 1, PersonnelGroupCount = @PersonnelGroupCount1
		
	IF @UseSecondGroup = 1
	BEGIN
	
		INSERT INTO #CompetenceSimulationData(AnswerDate, CompetenceValue, CompetenceName, PersonName, PersonId, CompetenceType)
		exec dbo.HRHouse_GetCompetenceSurveillanceData @Person2, @StartDate, @EndDate, @Sex2, @Company2, @BasicEducation2, @EducationLevel2, @FieldOfStudy2, @EmploymentRelationship2, @Superior2, @CompetenceOfPersonnel2, @CompetenceGroup, @CompetenceArea, @Competence, @PersonnelGroupCondition2 OUT
		
		SET @SQLString = @SQLStringTemplate + @PersonnelGroupCondition2	

		SET @SQLString = N'select @PersonnelGroupCount = PersonnelGroupCount from OPENQUERY(HRHOUSE_LINK, ''' +  REPLACE(@SQLString, '''', '''''') + ''')'

		EXEC sp_executesql @SQLString, @ParmDefinition, @PersonnelGroupCount = @PersonnelGroupCount2 OUTPUT	
		
		UPDATE #CompetenceSimulationData
		SET PersonnelGroup = 2, PersonnelGroupCount = @PersonnelGroupCount2
		WHERE PersonnelGroup is NULL
	END;
	
	WITH LatestAnswers (AnswerDate, PersonId, PersonnelGroup, CompetenceName) AS 
	(
		SELECT MAX(AnswerDate), PersonId, PersonnelGroup, CompetenceName
		FROM #CompetenceSimulationData
		GROUP BY PersonId, PersonnelGroup, CompetenceName
	)

	DELETE d1 FROM #CompetenceSimulationData d1, LatestAnswers d2 
	WHERE d1.AnswerDate < d2.AnswerDate AND
		d1.PersonId = d2.PersonId AND d1.PersonnelGroup = d2.PersonnelGroup AND d1.CompetenceName = d2.CompetenceName; 	

	SELECT PersonnelGroup, AVG(CompetenceValue) as CompetenceValue, CompetenceName, CompetenceType, COUNT(PersonId) as PersonCount, PersonnelGroupCount   
	FROM #CompetenceSimulationData
	GROUP BY PersonnelGroup, CompetenceName, CompetenceType, PersonnelGroupCount
	ORDER BY CompetenceName, PersonnelGroup
	
	DROP TABLE #CompetenceSimulationData
END