﻿CREATE PROCEDURE [dbo].[HR_House_GetSuperGroups]
@Language NVARCHAR (5)
AS
BEGIN
	
	CREATE TABLE #tmpGroups
	(
		[ID] int,
		[Text] varchar(100) 
	)
	
	IF @Language = 'fi-FI'
	BEGIN
	
		INSERT INTO #tmpGroups ([ID], [Text])
		SELECT -1 as [ID], 'Ei Filtteriä' As [Text]
		
	END
	ELSE 
	BEGIN
	
		INSERT INTO #tmpGroups ([ID], [Text])
		SELECT -1 as [ID], 'No Filtering' As [Text]
		
	END	
	
	INSERT INTO #tmpGroups ([ID], [Text])
	SELECT sg.id as [ID], sg.RagioneSociale as [Text] 
	FROM OrderManagement.dbo.joinSuperGroup sg
	ORDER BY 2	
	
	SELECT * FROM #tmpGroups
	
	DROP TABLE #tmpGroups
	
END