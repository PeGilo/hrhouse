﻿CREATE PROCEDURE [dbo].[HRHouse_ApplicationProfile_Gender]
(
	@BeginDate	DATETIME, 
	@EndDate	DATETIME,
	@Language	NVARCHAR(5)
)	
AS
BEGIN

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	IF (@BeginDate IS NULL)  SELECT @BeginDate = dbo.DateTimeMinValue()
	IF (@EndDate IS NULL)  SELECT @EndDate = dbo.DateTimeMaxValue()
	IF (@Language IS NULL)  SELECT @Language = 'en-US'

	-- FilterNumber, ParameterId, ParameterName, ParameterOrderBy, Cts, Value, Rank, GroupName
	SELECT	19,
			HAKEMUS.HAKEMUS_SUKUPUOLI,
			CASE HAKEMUS.HAKEMUS_SUKUPUOLI WHEN 1 THEN 'Male' WHEN 2 THEN 'Female' END,
			HAKEMUS.HAKEMUS_SUKUPUOLI,
			HAKEMUS.CTS,
			1,
			0, -- No ranks
			null
	FROM [$(RecruitmentDatabaseName)].dbo.REP_REKRY_HAKEMUS AS HAKEMUS
	WHERE HAKEMUS.CTS BETWEEN @BeginDate AND @EndDate
		AND HAKEMUS.HAKEMUS_SUKUPUOLI IN (1, 2)
	ORDER BY HAKEMUS.CTS
	
END