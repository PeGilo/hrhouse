﻿CREATE PROCEDURE [dbo].[HRHouse_ApplicationProfile_EducationLevels]
(
	@BeginDate	DATETIME, 
	@EndDate	DATETIME,
	@Language	NVARCHAR(5)
)	
AS
BEGIN

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	IF (@BeginDate IS NULL)  SELECT @BeginDate = dbo.DateTimeMinValue()
	IF (@EndDate IS NULL)  SELECT @EndDate = dbo.DateTimeMaxValue()
	IF (@Language IS NULL)  SELECT @Language = 'en-US'
	
	SELECT [DELE_ID], [LOCALIZED_NAME] INTO #DegreeLevelLocalized FROM dbo.[F_GetDegreeLevelTranslation](@Language) 
	
	-- FilterNumber, ParameterId, ParameterName, ParameterOrderBy, Cts, Value, Rank, GroupName
	SELECT 4,
			DISTINCT_DEGREES.DELE_ID,
			DL_LOCAL.LOCALIZED_NAME,
			DISTINCT_DEGREES.DELE_ORDER_BY,
			HAK.cts,
			1 as [Value],
			RANKING.[Rank],
			null
	FROM ( -- We want select only non-repeating EducationLevels for Applications
			SELECT DISTINCT HAKEMUS.ID, DL.DELE_ID,	DL.DELE_ORDER_BY
			FROM [$(RecruitmentDatabaseName)].dbo.REP_REKRY_HAKEMUS AS HAKEMUS
			JOIN [$(RecruitmentDatabaseName)].dbo.REP_APP_EDUCATION AS EDU ON HAKEMUS.ID = EDU.AED_APPLICATION_ID
			JOIN [$(RecruitmentDatabaseName)].dbo.REP_DELE_DEGREE_LEVEL AS DL ON DL.DELE_ID = EDU.AED_LEVEL_ID
			
			WHERE HAKEMUS.CTS BETWEEN @BeginDate AND @EndDate
		) AS DISTINCT_DEGREES
	-- Geting localization for degrees
	LEFT OUTER JOIN #DegreeLevelLocalized AS DL_LOCAL ON DISTINCT_DEGREES.DELE_ID = DL_LOCAL.DELE_ID
	-- As we should get CTS from HAKEMUS outside distinct, we have to join it.
	JOIN [$(RecruitmentDatabaseName)].dbo.REP_REKRY_HAKEMUS as HAK ON DISTINCT_DEGREES.ID = HAK.ID
	-- Ranking result set, so every Parameter has its rank by Count
	JOIN (		
		-- As rank is used row number in sorted table
		SELECT row_number() over( ORDER BY COUNT(DELE_ID) desc) as [Rank], DELE_ID 
		FROM(
			SELECT DISTINCT HAKEMUS.ID, DL.DELE_ID
			FROM [$(RecruitmentDatabaseName)].dbo.REP_REKRY_HAKEMUS AS HAKEMUS
			JOIN [$(RecruitmentDatabaseName)].dbo.REP_APP_EDUCATION AS EDU ON HAKEMUS.ID = EDU.AED_APPLICATION_ID
			JOIN [$(RecruitmentDatabaseName)].dbo.REP_DELE_DEGREE_LEVEL AS DL ON DL.DELE_ID = EDU.AED_LEVEL_ID
			WHERE HAKEMUS.CTS BETWEEN @BeginDate AND @EndDate
		) T
		GROUP BY DELE_ID
		) RANKING ON DISTINCT_DEGREES.DELE_ID = RANKING.DELE_ID
	ORDER BY HAK.CTS

END