﻿CREATE PROCEDURE [dbo].[HRHouse_GetBasicEducations]
@Language NVARCHAR (5)
AS
BEGIN
	SET NOCOUNT ON;

IF @Language = 'fi-FI'
	BEGIN
		SELECT -1 as Id, 'Ei Filtteriä' as Name
		UNION
		SELECT Id, Name
		FROM OPENQUERY(HRHOUSE_LINK, 'select leoe_id as Id, syte_fi as Name 
			from
				mpy.leoe_level_of_education,
				 mpy.syte_system_text
			where
				syte_key = Right(leoe_name, Length(leoe_name) - 1) And
				leoe_level_of_education_type_id = 10 and
				Left(leoe_name,1) = ''!''  
			union
			select leoe_id as Id, leoe_name as Name
			from
				mpy.leoe_level_of_education
			where 
				leoe_level_of_education_type_id = 10 And
				Left(leoe_name,1) <> ''!''') AS BasicEducations
		ORDER BY Id
	END
ELSE IF @Language = 'en-US'  
	BEGIN
		SELECT -1 as Id, 'No Filtering' as Name
		UNION
		SELECT Id, Name
		FROM OPENQUERY(HRHOUSE_LINK, 'select leoe_id as Id, syte_en as Name 
			from
				mpy.leoe_level_of_education,
				 mpy.syte_system_text
			where
				syte_key = Right(leoe_name, Length(leoe_name) - 1) And
				leoe_level_of_education_type_id = 10 and
				Left(leoe_name,1) = ''!''  
			union
			select leoe_id as Id, leoe_name as Name
			from
				mpy.leoe_level_of_education
			where 
				leoe_level_of_education_type_id = 10 And
				Left(leoe_name,1) <> ''!''') AS BasicEducations
		ORDER BY Id
	END
END


