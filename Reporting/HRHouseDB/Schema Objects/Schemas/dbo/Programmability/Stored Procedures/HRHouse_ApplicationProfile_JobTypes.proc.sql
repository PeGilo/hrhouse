﻿CREATE PROCEDURE [dbo].[HRHouse_ApplicationProfile_JobTypes]
(
	@BeginDate	DATETIME, 
	@EndDate	DATETIME,
	@Language	NVARCHAR(5)
)	
AS
BEGIN

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	IF (@BeginDate IS NULL)  SELECT @BeginDate = dbo.DateTimeMinValue()
	IF (@EndDate IS NULL)  SELECT @EndDate = dbo.DateTimeMaxValue()
	IF (@Language IS NULL)  SELECT @Language = 'en-US'

	-- FilterNumber, ParameterId, ParameterName, ParameterOrderBy, Cts, Value, Rank, GroupName
	select  15,
			DISTINCT_JOBS.TJ_ID,
			DISTINCT_JOBS.TJ_NAME,
			0, -- No order by column
			HAK.CTS,
			1,
			0, -- No ranks
			null	
	FROM
	(
	SELECT DISTINCT HAKEMUS.ID AS [HAKEMUS_ID],
			JOB_TYPE.TJ_ID,
			JOB_TYPE.TJ_NAME
	FROM [$(RecruitmentDatabaseName)].dbo.REP_REKRY_HAKEMUS AS HAKEMUS
	JOIN [$(RecruitmentDatabaseName)].dbo.REP_APP_JOB_TYPE AS APP_JOB_TYPE ON APP_JOB_TYPE.APT_APPLICATION_ID = HAKEMUS.ID
	JOIN [$(RecruitmentDatabaseName)].dbo.REP_JOB_TYPE AS JOB_TYPE ON JOB_TYPE.TJ_ID = APP_JOB_TYPE.APT_JOB_TYPE_ID
	WHERE HAKEMUS.CTS BETWEEN @BeginDate AND @EndDate
	) DISTINCT_JOBS
	-- As we should get CTS from HAKEMUS outside distinct, we have to join it.
	JOIN [$(RecruitmentDatabaseName)].dbo.REP_REKRY_HAKEMUS as HAK ON DISTINCT_JOBS.[HAKEMUS_ID] = HAK.ID
	ORDER BY HAK.CTS
		
END