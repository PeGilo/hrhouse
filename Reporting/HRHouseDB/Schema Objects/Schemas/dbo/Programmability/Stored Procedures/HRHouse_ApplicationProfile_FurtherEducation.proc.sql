﻿CREATE PROCEDURE [dbo].[HRHouse_ApplicationProfile_FurtherEducation]
(
	@BeginDate	DATETIME, 
	@EndDate	DATETIME,
	@Language	NVARCHAR(5)
)	
AS
BEGIN

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	IF (@BeginDate IS NULL)  SELECT @BeginDate = dbo.DateTimeMinValue()
	IF (@EndDate IS NULL)  SELECT @EndDate = dbo.DateTimeMaxValue()
	IF (@Language IS NULL)  SELECT @Language = 'en-US'

	-- FilterNumber, ParameterId, ParameterName, ParameterOrderBy, Cts, Value, Rank, GroupName
	SELECT	2,
			1,	-- ParameterId is one, so that can be any value
			'', -- Name is not shown, so empty
			0,
			HAKEMUS.cts,
			(select count(TAITO.TAITO_NIMI) from [$(RecruitmentDatabaseName)].dbo.REP_REKRY_HAKEMUKSEN_TAIDOT as HAK_TAIDOT
				join [$(RecruitmentDatabaseName)].dbo.REP_REKRY_TAITO as TAITO ON HAK_TAIDOT.TAITO_ID = TAITO.ID 
				AND TAITO.TAITO_NIMI = 'further_education'
				WHERE HAK_TAIDOT.HAKEMUS_ID = HAKEMUS.ID
			 ),
			 0,	-- No ranks
			 null
	FROM [$(RecruitmentDatabaseName)].dbo.REP_REKRY_HAKEMUS AS HAKEMUS
	WHERE HAKEMUS.CTS BETWEEN @BeginDate AND @EndDate
	ORDER BY HAKEMUS.CTS
		
END