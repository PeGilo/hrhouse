﻿CREATE PROCEDURE [dbo].[HRHouse_ConfirmedLateAndRapidOrder]
	@ReportPeriodType TINYINT, 
	@ReportDateFrom DATETIME, 
	@ReportDateTo DATETIME,
	@Scope NVARCHAR (MAX), 
	@CustomerId VARCHAR (8) = NULL, 
	@GroupId VARCHAR (8) = NULL, 
	@SuperGroupId INT = NULL, 
	@OnlyActiveCustomer BIT = 0
AS
BEGIN

	IF @CustomerId = '-1' SET @CustomerId = NULL
	IF @GroupId = '-1' SET @GroupId = NULL
	IF @SuperGroupId = -1 SET @SuperGroupId = NULL;
	
	SELECT DISTINCT 		 
		c1.customerpin		AS [CustomerID],
		sg.RagioneSociale	AS [SuperGroup], 
		c.RagioneSociale	AS [Group], 		
		c1.RagioneSociale	AS [CostCenter],
		(
			SELECT    COUNT(1)
			FROM      [$(OrderManagementDatabaseName)].dbo.prenotazioni p
			WHERE     (p.dataini >= r.Begindate) AND (p.dataini < r.Enddate) AND (p.locationpin = c1.customerpin) AND (p.fatturato = 'S') AND (p.ConfirmedLater <> 0)
		)					AS [ConfirmedLate],
		(
			SELECT    COUNT(1)
			FROM      [$(OrderManagementDatabaseName)].dbo.prenotazioni p
			WHERE	  (p.dataini >= r.Begindate) AND (p.dataini < r.Enddate) AND (p.locationpin = c1.customerpin) AND (p.RapidoPreno <> 0) 
		)					AS [RapidOrders],
		r.Begindate			AS [Range] 
	FROM	[$(OrderManagementDatabaseName)].dbo.joinSuperGroup sg 
			RIGHT OUTER JOIN [$(OrderManagementDatabaseName)].dbo.joinAdmGroupInvoice ag ON sg.id = ag.SuperGroupId			
			INNER JOIN [$(OrderManagementDatabaseName)].dbo.joinParentLocation  pl ON ag.GroupPin = pl.customerPin
			INNER JOIN [$(OrderManagementDatabaseName)].dbo.customer c ON c.customerpin = ag.GroupPin 
			INNER JOIN [$(OrderManagementDatabaseName)].dbo.customer AS c1 ON c1.customerpin = pl.customerChild
			CROSS APPLY dbo.F_GetDateRanges(@ReportDateFrom, @ReportDateTo, @ReportPeriodType, @Scope) r
	WHERE c1.customerpin = ISNULL(@CustomerId, c1.customerpin) AND
		  c.customerpin = ISNULL(@GroupId, c.customerpin) AND
		  (@SuperGroupId is NULL OR sg.id = @SuperGroupId) AND
		  (@OnlyActiveCustomer = 0 OR c1.active = 1)		
	ORDER BY 2, 3, 4, 7
	OPTION (RECOMPILE)
			
END