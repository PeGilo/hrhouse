﻿CREATE PROCEDURE [dbo].[HRHouse_ApplicationProfile_CurrentSituation]
(
	@BeginDate	DATETIME, 
	@EndDate	DATETIME,
	@Language	NVARCHAR(5)
)	
AS
BEGIN

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	IF (@BeginDate IS NULL)  SELECT @BeginDate = dbo.DateTimeMinValue()
	IF (@EndDate IS NULL)  SELECT @EndDate = dbo.DateTimeMaxValue()
	IF (@Language IS NULL)  SELECT @Language = 'en-US'

	-- FilterNumber, ParameterId, ParameterName, ParameterOrderBy, Cts, Value, Rank, GroupName
	SELECT	5,
			CASE TAITO.TAITO_NIMI 
				WHEN 'currently_student' THEN 1
				WHEN 'currently_working' THEN 2	
				WHEN 'currently_not_working' THEN 3
				WHEN 'currently_other' THEN 4	
			END as 'ParameterId',			
			TAITO.TAITO_NIMI, 
			0,
			HAKEMUS.cts,
			1,
			0, -- No ranks
			null
	FROM [$(RecruitmentDatabaseName)].dbo.REP_REKRY_HAKEMUS AS HAKEMUS
	JOIN [$(RecruitmentDatabaseName)].dbo.REP_REKRY_HAKEMUKSEN_TAIDOT as HAK_TAIDOT ON HAKEMUS.ID = HAK_TAIDOT.HAKEMUS_ID
	JOIN [$(RecruitmentDatabaseName)].dbo.REP_REKRY_TAITO as TAITO ON HAK_TAIDOT.TAITO_ID = TAITO.ID 
					AND (TAITO.TAITO_NIMI = 'currently_student' OR TAITO.TAITO_NIMI = 'currently_working' 
					 OR TAITO.TAITO_NIMI = 'currently_not_working' 
					 OR TAITO.TAITO_NIMI = 'currently_other')
	WHERE HAKEMUS.CTS BETWEEN @BeginDate AND @EndDate
	ORDER BY HAKEMUS.CTS
		
END