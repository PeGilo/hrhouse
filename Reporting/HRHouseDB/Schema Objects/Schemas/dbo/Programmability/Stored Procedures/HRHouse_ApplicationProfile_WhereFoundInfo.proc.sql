﻿CREATE PROCEDURE [dbo].[HRHouse_ApplicationProfile_WhereFoundInfo]
(
	@BeginDate	DATETIME, 
	@EndDate	DATETIME,
	@Language	NVARCHAR(5)
)	
AS
BEGIN

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	IF (@BeginDate IS NULL)  SELECT @BeginDate = dbo.DateTimeMinValue()
	IF (@EndDate IS NULL)  SELECT @EndDate = dbo.DateTimeMaxValue()
	IF (@Language IS NULL)  SELECT @Language = 'en-US'

	-- FilterNumber, ParameterId, ParameterName, ParameterOrderBy, Cts, Value, Rank, GroupName
	select  21,
			WHERE_FOUND.W_ID,
			WHERE_FOUND.W_NAME,
			WHERE_FOUND.W_ORDER_BY,
			HAKEMUS.CTS,
			1,
			RANKING.[rank],
			null	

	FROM [$(RecruitmentDatabaseName)].dbo.REP_REKRY_HAKEMUS AS HAKEMUS
	JOIN [$(RecruitmentDatabaseName)].dbo.REP_WHERE_FOUND_OUT_OPTION AS WHERE_FOUND ON WHERE_FOUND.W_ID = HAKEMUS.WHERE_FOUND_OUT_OPTION_ID
	-- Ranking result set, so every Parameter has its rank by Count
	JOIN (		
		-- As rank is used row number in sorted table
		SELECT row_number() over( ORDER BY COUNT([W_ID]) desc) as [Rank], [W_ID] 
		FROM (
				SELECT WHERE_FOUND.W_ID
				FROM [$(RecruitmentDatabaseName)].dbo.REP_REKRY_HAKEMUS AS HAKEMUS
				JOIN [$(RecruitmentDatabaseName)].dbo.REP_WHERE_FOUND_OUT_OPTION AS WHERE_FOUND 
					ON WHERE_FOUND.W_ID = HAKEMUS.WHERE_FOUND_OUT_OPTION_ID
				WHERE HAKEMUS.CTS BETWEEN @BeginDate AND @EndDate
		) T
		GROUP BY [W_ID]
		) RANKING ON WHERE_FOUND.W_ID = RANKING.[W_ID]
	WHERE HAKEMUS.CTS BETWEEN @BeginDate AND @EndDate	
	ORDER BY HAKEMUS.CTS
		
END