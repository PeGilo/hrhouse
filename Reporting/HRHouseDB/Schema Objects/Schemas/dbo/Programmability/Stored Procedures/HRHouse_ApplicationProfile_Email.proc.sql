﻿CREATE PROCEDURE [dbo].[HRHouse_ApplicationProfile_Email]
(
	@BeginDate	DATETIME, 
	@EndDate	DATETIME,
	@Language	NVARCHAR(5)
)	
AS
BEGIN

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	IF (@BeginDate IS NULL)  SELECT @BeginDate = dbo.DateTimeMinValue()
	IF (@EndDate IS NULL)  SELECT @EndDate = dbo.DateTimeMaxValue()
	IF (@Language IS NULL)  SELECT @Language = 'en-US'
	

	-- FilterNumber, ParameterId, ParameterName, ParameterOrderBy, Cts, Value, Rank, GroupName
	select  11,
			(
				select 
					 CASE WHEN COUNT(TAITO.TAITO_NIMI) > 0 THEN MAX(HAK_TAIDOT.TAITOTASO_ID) ELSE 0 END
				from [$(RecruitmentDatabaseName)].dbo.REP_REKRY_HAKEMUKSEN_TAIDOT as HAK_TAIDOT
				join [$(RecruitmentDatabaseName)].dbo.REP_REKRY_TAITO as TAITO ON HAK_TAIDOT.TAITO_ID = TAITO.ID 
				AND TAITO.TAITO_NIMI = N'Email'
				-- AND TAITO.ID = 100662	
				WHERE HAK_TAIDOT.HAKEMUS_ID = HAKEMUS.ID
			 ),
			null, --'EmailSkill_' + CAST(HAK_TAIDOT.TAITOTASO_ID AS VARCHAR(2)), -- Needs translating in report
			null, --HAK_TAIDOT.TAITOTASO_ID, -- Order by Level
			HAKEMUS.CTS,
			1,
			0, -- No rank
			null
	FROM [$(RecruitmentDatabaseName)].dbo.REP_REKRY_HAKEMUS as HAKEMUS
    WHERE HAKEMUS.CTS BETWEEN @BeginDate AND @EndDate
    ORDER BY HAKEMUS.CTS		
	
END