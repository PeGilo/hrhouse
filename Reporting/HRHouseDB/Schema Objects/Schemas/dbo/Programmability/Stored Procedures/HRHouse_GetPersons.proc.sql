﻿CREATE PROCEDURE [dbo].[HRHouse_GetPersons]
@Language NVARCHAR (5), @Company INT
AS
BEGIN
	SET NOCOUNT ON;

	Declare @NameText nvarchar(max)
	
	if @Language = 'fi-FI'
		Set @NameText = 'Ei Filtteriä'
	Else If @Language = 'en-US'
		Set @NameText = 'No Filtering'

-- If no company selected
If @Company = -1
	Begin
		SELECT -1 as Id, @NameText As Name
		UNION
		SELECT Id, Name
		FROM OPENQUERY(HRHOUSE_LINK, 'select pede_id as Id, pede_full_name as Name from pede_personal_detail') AS Persons
		ORDER BY Id
	End
Else -- Company has been selected
	Begin
	
		Declare @SQLString nvarchar(max)
		
		Set @SQLString = 'SELECT distinct pede_personal_detail.pede_id as Id, pede_full_name as Name
			FROM
				mpy.pede_personal_detail,
				mpy.emrp_employment_relationship_person,
				mpy.emre_employment_relationship,
				mpy.emre_grou
			WHERE
				pede_personal_detail.pede_id = emrp_employment_relationship_person.emrp_personal_detail_id AND
				emrp_employment_relationship_person.emrp_employment_relationship_id = emre_employment_relationship.emre_id AND
				emre_employment_relationship.emre_id = emre_grou.emre_id AND
				emre_employment_relationship.emre_start_date <= CURDATE() AND
				(emre_employment_relationship.emre_end_date is null or emre_employment_relationship.emre_end_date >= CURDATE()) AND
				emre_grou.grou_id = ' + Cast(@Company As varchar(20))
		
		SET @SQLString = N' SELECT -1 as Id, ''' +  @NameText + ''' As Name
						UNION
						select * from OPENQUERY(HRHOUSE_LINK, ''' + REPLACE(@SQLString, '''', '''''') + ''') Order By Id' 
									
		Exec(@SQLString)
			
	End
END


