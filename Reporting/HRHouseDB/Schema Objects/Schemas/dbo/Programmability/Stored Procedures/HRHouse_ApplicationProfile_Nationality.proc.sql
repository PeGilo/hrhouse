﻿CREATE PROCEDURE [dbo].[HRHouse_ApplicationProfile_Nationality]
(
	@BeginDate	DATETIME, 
	@EndDate	DATETIME,
	@Language	NVARCHAR(5)
)	
AS
BEGIN

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	IF (@BeginDate IS NULL)  SELECT @BeginDate = dbo.DateTimeMinValue()
	IF (@EndDate IS NULL)  SELECT @EndDate = dbo.DateTimeMaxValue()
	IF (@Language IS NULL)  SELECT @Language = 'en-US'

	-- FilterNumber, ParameterId, ParameterName, ParameterOrderBy, Cts, Value, Rank, GroupName
	select  17,
			CITIZENSHIP.CITI_ID,
			CITIZENSHIP.CITI_NAME,
			CITIZENSHIP.CITI_ORDER_BY,
			HAKEMUS.CTS,
			1,
			RANKING.[rank],
			null	

	FROM [$(RecruitmentDatabaseName)].dbo.REP_REKRY_HAKEMUS AS HAKEMUS
	JOIN [$(RecruitmentDatabaseName)].dbo.REP_CITI_CITIZENSHIP AS CITIZENSHIP ON CITIZENSHIP.CITI_ID = HAKEMUS.HAKEMUS_CITIZENSHIP_ID
	-- Ranking result set, so every Parameter has its rank by Count
	JOIN (		
		-- As rank is used row number in sorted table
		SELECT row_number() over( ORDER BY COUNT([CITI_ID]) desc) as [Rank], [CITI_ID] 
		FROM (
				SELECT CITIZENSHIP.CITI_ID
				FROM [$(RecruitmentDatabaseName)].dbo.REP_REKRY_HAKEMUS AS HAKEMUS
				JOIN [$(RecruitmentDatabaseName)].dbo.REP_CITI_CITIZENSHIP AS CITIZENSHIP 
					ON CITIZENSHIP.CITI_ID = HAKEMUS.HAKEMUS_CITIZENSHIP_ID
				WHERE HAKEMUS.CTS BETWEEN @BeginDate AND @EndDate
		) T
		GROUP BY [CITI_ID]
		) RANKING ON CITIZENSHIP.CITI_ID = RANKING.[CITI_ID]
	WHERE HAKEMUS.CTS BETWEEN @BeginDate AND @EndDate		
	ORDER BY HAKEMUS.CTS
		
END