﻿CREATE PROCEDURE [dbo].[HRHouse_GetCompetenceAreas]
@Language NVARCHAR (5)
AS
BEGIN
	SET NOCOUNT ON;

If @Language = 'fi-FI'
	Begin
		SELECT -1 as Id, 'Ei Valintaa' as Name
		UNION
		SELECT Id, Name
		FROM OPENQUERY(HRHOUSE_LINK, 'select comp_id as Id, comp_name as Name from comp_competence where comp_compotence_type_id = 20') AS CompetenceAreas
		ORDER BY Id
	End
Else If @Language = 'en-US'
	Begin
		SELECT -1 as Id, 'None' as Name
		UNION
		SELECT Id, Name
		FROM OPENQUERY(HRHOUSE_LINK, 'select comp_id as Id, comp_name as Name from comp_competence where comp_compotence_type_id = 20') AS CompetenceAreas
		ORDER BY Id
	End
	 
END


