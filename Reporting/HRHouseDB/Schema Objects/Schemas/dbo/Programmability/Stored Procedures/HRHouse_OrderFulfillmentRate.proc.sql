﻿CREATE PROCEDURE [dbo].[HRHouse_OrderFulfillmentRate]
	@ReportPeriodType TINYINT, 
	@ReportDateFrom DATETIME, 
	@ReportDateTo DATETIME, 
	@Scope NVARCHAR (MAX), 
	@CustomerId VARCHAR (8) = NULL, 
	@GroupId VARCHAR (8) = NULL, 
	@SuperGroupId INT = NULL, 
	@OnlyActiveCustomer BIT = 0
AS
BEGIN

	DECLARE @StartPeriod datetime, @EndPeriod datetime
	DECLARE @PeriodAnount int
	DECLARE @ReportTable TABLE (CustomerID varchar(8), SuperGroup varchar(100), [Group] varchar(100), CostCenter varchar(100), Orders int, NotFound int, [Range] datetime)
	
	IF @CustomerId = '-1' SET @CustomerId = NULL
	IF @GroupId = '-1' SET @GroupId = NULL
	IF @SuperGroupId = -1 SET @SuperGroupId = NULL	

	INSERT INTO @ReportTable 
	SELECT DISTINCT 		 
		c1.customerpin		AS [CustomerID],
		sg.RagioneSociale	AS [SuperGroup], 
		c.RagioneSociale	AS [Group], 		
		c1.RagioneSociale	AS [CostCenter],
		(
			SELECT    COUNT(1)
			FROM      [$(OrderManagementDatabaseName)].dbo.prenotazioni p
			WHERE     (p.dataini >= r.Begindate) AND (p.dataini < r.Enddate) AND (p.locationpin = c1.customerpin) AND						
					  ((p.cancellato = 'N') OR NOT (											/* Orders that are not deleted (We count it as an order as long as its status is not cancelled) */						
					   /*(p.fatturato = 'S') OR */													/* Invoiced */
					   ((p.idnome IS NULL) AND (p.cancellato = 'S') AND (p.WhyDeleted = 3)) OR 	/* Orders that are deleted and marked as Not found */ 
					   ((p.cancellato = 'S') AND (p.WhyDeleted = 2)))							/* Orders that are deleted and marked as Customer cancelled */
					  )
		)					AS Orders,
		(
			SELECT    COUNT(1)
			FROM      [$(OrderManagementDatabaseName)].dbo.prenotazioni p
			WHERE     (p.dataini >= r.Begindate) AND (p.dataini < r.Enddate) AND (p.locationpin = c1.customerpin) AND
					  (p.idnome IS NULL) AND (p.cancellato = 'S') AND (p.WhyDeleted = 3) 
		)					AS NotFound,
		r.Begindate			AS [Range] 
	FROM	[$(OrderManagementDatabaseName)].dbo.joinSuperGroup sg 
			RIGHT OUTER JOIN [$(OrderManagementDatabaseName)].dbo.joinAdmGroupInvoice ag ON sg.id = ag.SuperGroupId			
			INNER JOIN [$(OrderManagementDatabaseName)].dbo.joinParentLocation  pl ON ag.GroupPin = pl.customerPin
			INNER JOIN [$(OrderManagementDatabaseName)].dbo.customer c ON c.customerpin = ag.GroupPin 
			INNER JOIN [$(OrderManagementDatabaseName)].dbo.customer AS c1 ON c1.customerpin = pl.customerChild
			CROSS APPLY dbo.F_GetDateRanges(@ReportDateFrom, @ReportDateTo, @ReportPeriodType, @Scope) r
	WHERE c1.customerpin = ISNULL(@CustomerId, c1.customerpin) AND
		  c.customerpin = ISNULL(@GroupId, c.customerpin) AND
		  (@SuperGroupId is NULL OR sg.id = @SuperGroupId) AND
		  (@OnlyActiveCustomer = 0 OR c1.active = 1)				
	OPTION (RECOMPILE)	
	
	SELECT 
		CustomerID, 
		SuperGroup, 
		[Group], 
		CostCenter, 
		Orders, 
		NotFound,
		ROUND(CASE WHEN Orders = 0 THEN 0. ELSE 100. - NotFound * 100./Orders END, 2) AS Rate, 
		[Range]
	FROM  @ReportTable 
	ORDER BY 2, 3, 4, 8		
	
END