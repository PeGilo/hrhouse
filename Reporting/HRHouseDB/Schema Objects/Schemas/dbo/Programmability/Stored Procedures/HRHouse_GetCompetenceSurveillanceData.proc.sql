﻿CREATE PROCEDURE [dbo].[HRHouse_GetCompetenceSurveillanceData] 
	@Person nvarchar(max),
	@StartDate Date,
	@EndDate Date,
	@Sex int,
	@Company int,
	@BasicEducation int,
	@EducationLevel int,
	@FieldOfStudy int,
	@EmploymentRelationship varchar(max),
	@Superior int,
	@CompetenceOfPersonnel int,
	@CompetenceGroup nvarchar(max),
	@CompetenceArea nvarchar(max),
	@Competence nvarchar(max),
	@PersonnelGroupCondition nvarchar(max) = NULL OUT
AS
BEGIN
	SET NOCOUNT ON;

	Declare @SQLStringArea nvarchar(max)
	Declare @SQLStringGroup nvarchar(max)
	Declare @AdditionalWhere nvarchar(max)
	Declare @SQLString nvarchar(max)
	Declare @SQLMore nvarchar(max)

	Set @AdditionalWhere = ''
	Set @SQLString = ''
	Set @SQLStringArea = ''
	Set @SQLStringGroup = ''
				
	-- *******************************************************************
	-- Create additional where clause based on parameters passed in call
	-- *******************************************************************	
							
	-- Add person if selected ANY (multiselect)
	if (@Person <> '-1')
	Begin
		Set @SQLMore = 'SELECT pede_id FROM mpy.pede_personal_detail WHERE pede_id  IN (' + @Person + ')'
		Set @AdditionalWhere = ' AND pede_personal_detail.pede_id IN (' + @SQLMore + ')'		
	End

	-- Add sex if selected ANY
	if (@Sex <> 0)
	Begin
		Set @SQLMore = 'SELECT pede_id FROM mpy.pede_personal_detail WHERE pede_sex_id  = ' + Cast(@Sex As varchar(20))
		Set @AdditionalWhere = @AdditionalWhere + ' AND pede_personal_detail.pede_id IN (' + @SQLMore + ')'		
	End
	
	-- Create SQL to get all persons based on field of study
	if (@FieldOfStudy <> -1)
	Begin
		Set @SQLMore = 'SELECT pede_personal_detail.pede_id
			FROM
				mpy.pede_personal_detail,
				mpy.appl_application,
				mpy.educ_education
			WHERE
				pede_personal_detail.pede_id = appl_application.appl_personal_detail_id AND
				appl_application.appl_id = educ_education.educ_application_id AND
				educ_education.educ_line_of_business_id = ' + Cast(@FieldOfStudy As varchar(20))
		
		Set @AdditionalWhere = @AdditionalWhere + ' AND pede_personal_detail.pede_id IN (' + @SQLMore + ')'
	End		

    -- Add selected employment relationship parameters (multiselect)
    if (@EmploymentRelationship <> '-1')
    Begin
		Set @SQLMore = 'SELECT pede_personal_detail.pede_id
			FROM
				mpy.pede_personal_detail,
				mpy.emrp_employment_relationship_person,
				mpy.emre_employment_relationship,
				mpy.erjd_employment_relationship_job_detail
			WHERE
				pede_personal_detail.pede_id = emrp_employment_relationship_person.emrp_personal_detail_id AND
				emrp_employment_relationship_person.emrp_employment_relationship_id = emre_employment_relationship.emre_id AND
				emre_employment_relationship.emre_id = erjd_employment_relationship_job_detail.erjd_employment_relationship_id AND
				emre_employment_relationship.emre_start_date <= CURDATE() AND
				(emre_employment_relationship.emre_end_date is null or emre_employment_relationship.emre_end_date >= CURDATE()) AND
				erjd_employment_relationship_job_detail.erjd_job_detail_id IN (' + @EmploymentRelationship +  ')'
				
		Set @AdditionalWhere = @AdditionalWhere + ' AND pede_personal_detail.pede_id IN (' + @SQLMore + ')'
    End

	-- Create SQL to get all persons based on education level
	if (@EducationLevel <> -1)
	Begin
		Set @SQLMore = 'SELECT pede_personal_detail.pede_id
			FROM
				mpy.pede_personal_detail,
				mpy.appl_application,
				mpy.educ_education
			WHERE
				pede_personal_detail.pede_id = appl_application.appl_personal_detail_id AND
				appl_application.appl_id = educ_education.educ_application_id AND
				educ_education.educ_level_of_education_id = ' + Cast(@EducationLevel As varchar(20))
		
		Set @AdditionalWhere = @AdditionalWhere + ' AND pede_personal_detail.pede_id IN (' + @SQLMore + ')'
	End		

	-- Create SQL to get all persons based on basic education
	if (@BasicEducation <> -1)
	Begin
		Set @SQLMore = 'SELECT pede_personal_detail.pede_id
			FROM
				mpy.pede_personal_detail,
				mpy.appl_application
			WHERE
				pede_personal_detail.pede_id = appl_application.appl_personal_detail_id AND
				appl_application.appl_level_of_education_id = ' + Cast(@BasicEducation As varchar(20))
		
		Set @AdditionalWhere = @AdditionalWhere + ' AND pede_personal_detail.pede_id IN (' + @SQLMore + ')'
	End		

	-- Create SQL to get all persons based on company			
	if (@Company <> -1)
	Begin
		Set @SQLMore = 'SELECT pede_personal_detail.pede_id
			FROM
				mpy.pede_personal_detail,
				mpy.emrp_employment_relationship_person,
				mpy.emre_employment_relationship,
				mpy.emre_grou
			WHERE
				pede_personal_detail.pede_id = emrp_employment_relationship_person.emrp_personal_detail_id AND
				emrp_employment_relationship_person.emrp_employment_relationship_id = emre_employment_relationship.emre_id AND
				emre_employment_relationship.emre_id = emre_grou.emre_id AND
				emre_employment_relationship.emre_start_date <= CURDATE() AND
				(emre_employment_relationship.emre_end_date is null or emre_employment_relationship.emre_end_date >= CURDATE()) AND
				emre_grou.grou_id = ' + Cast(@Company As varchar(20))
				
		Set @AdditionalWhere = @AdditionalWhere + ' AND pede_personal_detail.pede_id IN (' + @SQLMore + ')'
	end
	
	-- Create SQL to get all persons based on selected superior
	if (@Superior <> -1)
	Begin
			Set @SQLMore = 'SELECT empl_employee.empl_personal_detail_id 
				FROM
					mpy.emrp_employment_relationship_person,
					mpy.emre_employment_relationship,
					mpy.empl_employee
				WHERE
					emrp_employment_relationship_person.emrp_employment_relationship_id = emre_employment_relationship.emre_id AND
					emre_employment_relationship.emre_employee_id = empl_employee.empl_id AND
					emrp_employment_relationship_person.emrp_employment_relationship_person_type_id = 20 AND
					emrp_employment_relationship_person.emrp_personal_detail_id = ' + Cast(@Superior As varchar(20))
			
			Set @AdditionalWhere = @AdditionalWhere + ' AND pede_personal_detail.pede_id IN (' + @SQLMore + ')'
	End	
	
	SET @PersonnelGroupCondition = @AdditionalWhere
	
	-- Competence Of personnel
	if (@CompetenceOfPersonnel <> -1)
	Begin
		Set @AdditionalWhere = @AdditionalWhere + ' AND coia_competence_inquiry_answer.coia_competence_inquiry_answer_type_id  = ' + Cast(@CompetenceOfPersonnel As varchar(20))
	End
	
	-- ***********************************************************************
	-- Create SQLs to get competences, competence areas and competence groups
	-- ***********************************************************************

	If (@Competence <> '-1')
	Begin						
		-- Basic query to get data
		Set @SQLString = 'SELECT coia_competence_inquiry_answer.coia_cts as AnswerDate,
				cole_competence_level.cole_value as CompetenceValue,
				comp_competence.comp_name as CompetenceName,
				pede_personal_detail.pede_first_name as PersonName,
				pede_personal_detail.pede_id as PersonId,
				3 as CompetenceType
			FROM 
				mpy.coia_competence_inquiry_answer,
				mpy.cole_competence_level,
				mpy.coqe_competence_question,
				mpy.comp_coqe,
				mpy.comp_competence,
				mpy.coip_competence_inquiry_person,
				mpy.pede_personal_detail
			WHERE
				coia_competence_inquiry_answer.coia_competence_level_id = cole_competence_level.cole_id
				AND coia_competence_inquiry_answer.coia_competence_question_id = coqe_competence_question.coqe_id
				AND coqe_competence_question.coqe_id = comp_coqe.coqe_id
				AND comp_coqe.comp_id = comp_competence.comp_id
				AND coip_competence_inquiry_person.coip_id = coia_competence_inquiry_answer.coia_competence_inquiry_person_id
				AND coip_competence_inquiry_person.coip_personal_detail_id = pede_personal_detail.pede_id
				AND comp_competence.comp_id IN (' + @Competence + ')
				AND coia_competence_inquiry_answer.coia_cts between ''' + CONVERT(CHAR(10), @StartDate, 23) + ''' AND ''' + CONVERT(CHAR(10), @EndDate, 23) + '''';
		
		Set @SQLString = @SQLString + @AdditionalWhere
	
	End
	
	-- If competence areas are selected add new query (with union if needed)
	if (@CompetenceArea <> '-1')
	Begin
	
		Set @SQLStringArea ='SELECT coia_competence_inquiry_answer.coia_cts as AnswerDate,
			cole_competence_level.cole_value as CompetenceValue,
			Concat(CompetenceArea.comp_name, '' (A)'') as CompetenceName,
			pede_personal_detail.pede_first_name as PersonName,
			pede_personal_detail.pede_id as PersonId,
			2 as CompetenceType
		FROM 
			mpy.coia_competence_inquiry_answer,
			mpy.cole_competence_level,
			mpy.coqe_competence_question,
			mpy.comp_coqe,
			mpy.comp_competence Competence,
			mpy.comp_competence CompetenceArea,
			mpy.coip_competence_inquiry_person,
			mpy.pede_personal_detail
		WHERE
			coia_competence_inquiry_answer.coia_competence_level_id = cole_competence_level.cole_id
      AND Competence.comp_parent_id = CompetenceArea.comp_id
			AND coia_competence_inquiry_answer.coia_competence_question_id = coqe_competence_question.coqe_id
			AND coqe_competence_question.coqe_id = comp_coqe.coqe_id
			AND comp_coqe.comp_id = Competence.comp_id
			AND coip_competence_inquiry_person.coip_id = coia_competence_inquiry_answer.coia_competence_inquiry_person_id
			AND coip_competence_inquiry_person.coip_personal_detail_id = pede_personal_detail.pede_id
			AND CompetenceArea.comp_id in (' + @CompetenceArea + ')
			AND coia_competence_inquiry_answer.coia_cts between ''' + CONVERT(CHAR(10), @StartDate, 23) + ''' AND ''' + CONVERT(CHAR(10), @EndDate, 23) + '''';
	
		Set @SQLStringArea = @SQLStringArea + @AdditionalWhere
		
		-- Create union if any competences have been selected, otherwise just areas
		If (@SQLString <> '')
			Set @SQLString = @SQLString + ' UNION ' + @SQLStringArea
		Else
			Set @SQLString = @SQLStringArea
			
	End	
	
	-- If competence groups are selected add new query (with union if needed)
	if (@CompetenceGroup <> '-1')
	Begin
	
		Set @SQLStringGroup ='SELECT coia_competence_inquiry_answer.coia_cts as AnswerDate,
			cole_competence_level.cole_value as CompetenceValue,
			Concat(CompetenceGroup.comp_name, '' (G)'') as CompetenceName,
			pede_personal_detail.pede_first_name as PersonName,
			pede_personal_detail.pede_id as PersonId,
			1 as CompetenceType
		FROM 
			mpy.coia_competence_inquiry_answer,
			mpy.cole_competence_level,
			mpy.coqe_competence_question,
			mpy.comp_coqe,
			mpy.comp_competence Competence,
			mpy.comp_competence CompetenceArea,
			mpy.comp_competence CompetenceGroup,
			mpy.coip_competence_inquiry_person,
			mpy.pede_personal_detail
		WHERE
			coia_competence_inquiry_answer.coia_competence_level_id = cole_competence_level.cole_id
      AND Competence.comp_parent_id = CompetenceArea.comp_id
      AND CompetenceArea.comp_parent_id = CompetenceGroup.comp_id
			AND coia_competence_inquiry_answer.coia_competence_question_id = coqe_competence_question.coqe_id
			AND coqe_competence_question.coqe_id = comp_coqe.coqe_id
			AND comp_coqe.comp_id = Competence.comp_id
			AND coip_competence_inquiry_person.coip_id = coia_competence_inquiry_answer.coia_competence_inquiry_person_id
			AND coip_competence_inquiry_person.coip_personal_detail_id = pede_personal_detail.pede_id
			AND CompetenceGroup.comp_id in (' + @CompetenceGroup + ')
			AND coia_competence_inquiry_answer.coia_cts between ''' + CONVERT(CHAR(10), @StartDate, 23) + ''' AND ''' + CONVERT(CHAR(10), @EndDate, 23) + '''';
	
		Set @SQLStringGroup = @SQLStringGroup + @AdditionalWhere
		
		-- Create union if any competences have been selected, otherwise just areas
		If (@SQLString <> '')
			Set @SQLString = @SQLString + ' UNION ' + @SQLStringGroup
		Else
			Set @SQLString = @SQLStringGroup
			
	End	
	
	insert into HRHouse.dbo.LogTable(SQLText) Values (@SQLString)

	-- **********************
	-- Execute generated SQL
	-- **********************	
	If (@SQLString <> '')
	Begin
		SET @SQLString = N'select * from OPENQUERY(HRHOUSE_LINK, ''' + REPLACE(@SQLString, '''', '''''') + ''')' 			
		Exec(@SQLString)
	End
	
END