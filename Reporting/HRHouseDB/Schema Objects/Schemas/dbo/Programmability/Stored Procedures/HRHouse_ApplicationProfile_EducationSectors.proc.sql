﻿CREATE PROCEDURE [dbo].[HRHouse_ApplicationProfile_EducationSectors]
(
	@BeginDate	DATETIME, 
	@EndDate	DATETIME,
	@Language	NVARCHAR(5)
)	
AS
BEGIN

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	IF (@BeginDate IS NULL)  SELECT @BeginDate = dbo.DateTimeMinValue()
	IF (@EndDate IS NULL)  SELECT @EndDate = dbo.DateTimeMaxValue()
	IF (@Language IS NULL)  SELECT @Language = 'en-US'
	
	SELECT [DETY_ID], [LOCALIZED_NAME] INTO #DegreeTypeLocalized FROM dbo.[F_GetDegreeTypeTranslation](@Language) 
	

	-- FilterNumber, ParameterId, ParameterName, ParameterOrderBy, Cts, Value, Rank, GroupName
	SELECT  3,
			DISTINCT_DEGREES.DETY_ID,
			DT_LOCAL.LOCALIZED_NAME,
			DISTINCT_DEGREES.DETY_ORDER_BY,
			HAK.cts,
			1 as [Value],
			RANKING.[Rank],
			null
	FROM ( -- We want select only non-repeating EducationSectors for Applications
			SELECT DISTINCT HAKEMUS.ID, DT.DETY_ID,	DT.DETY_ORDER_BY
			FROM [$(RecruitmentDatabaseName)].dbo.REP_REKRY_HAKEMUS AS HAKEMUS
			JOIN [$(RecruitmentDatabaseName)].dbo.REP_APP_EDUCATION AS EDU ON HAKEMUS.ID = EDU.AED_APPLICATION_ID
			JOIN [$(RecruitmentDatabaseName)].dbo.REP_DETY_DEGREE_TYPE AS DT ON DT.DETY_ID = EDU.AED_TYPE_ID
			
			WHERE HAKEMUS.CTS BETWEEN @BeginDate AND @EndDate
		) AS DISTINCT_DEGREES
	-- Geting localization for degrees
	LEFT OUTER JOIN #DegreeTypeLocalized AS DT_LOCAL ON DISTINCT_DEGREES.DETY_ID = DT_LOCAL.DETY_ID
				--(SELECT DETY_ID, LOCALIZED_NAME FROM dbo.[F_GetDegreeTypeTranslation](@Language)) as DT_LOCAL
	-- As we should get CTS from HAKEMUS outside distinct, we have to join it.
	JOIN [$(RecruitmentDatabaseName)].dbo.REP_REKRY_HAKEMUS as HAK ON DISTINCT_DEGREES.ID = HAK.ID
	-- Ranking result set, so every Parameter has its rank by Count
	JOIN (		
		-- As rank is used row number in sorted table
		SELECT row_number() over( ORDER BY COUNT(DETY_ID) desc) as [Rank], DETY_ID 
		FROM(
			SELECT DISTINCT HAKEMUS.ID, DT.DETY_ID
			FROM [$(RecruitmentDatabaseName)].dbo.REP_REKRY_HAKEMUS AS HAKEMUS
			JOIN [$(RecruitmentDatabaseName)].dbo.REP_APP_EDUCATION AS EDU ON HAKEMUS.ID = EDU.AED_APPLICATION_ID
			JOIN [$(RecruitmentDatabaseName)].dbo.REP_DETY_DEGREE_TYPE AS DT ON DT.DETY_ID = EDU.AED_TYPE_ID
			WHERE HAKEMUS.CTS BETWEEN @BeginDate AND @EndDate
		) T
		GROUP BY DETY_ID
		) RANKING ON DISTINCT_DEGREES.DETY_ID = RANKING.DETY_ID
	ORDER BY HAK.CTS

END