﻿CREATE PROCEDURE [dbo].[HRHouse_ApplicationProfile_AfterSalesPermission]
(
	@BeginDate	DATETIME, 
	@EndDate	DATETIME,
	@Language	NVARCHAR(5)
)	
AS
BEGIN

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	IF (@BeginDate IS NULL)  SELECT @BeginDate = dbo.DateTimeMinValue()
	IF (@EndDate IS NULL)  SELECT @EndDate = dbo.DateTimeMaxValue()
	IF (@Language IS NULL)  SELECT @Language = 'en-US'

	-- FilterNumber, ParameterId, ParameterName, ParameterOrderBy, Cts, Value, Rank, GroupName
	SELECT	18,
			HAKEMUS.AFTER_SALES_PERMISSION,
			CASE HAKEMUS.AFTER_SALES_PERMISSION WHEN 1 THEN 'Yes' WHEN 0 THEN 'No' END,
			HAKEMUS.AFTER_SALES_PERMISSION,
			HAKEMUS.cts,
			1,
			0, -- No ranks
			null
	FROM [$(RecruitmentDatabaseName)].dbo.REP_REKRY_HAKEMUS AS HAKEMUS
	WHERE HAKEMUS.CTS BETWEEN @BeginDate AND @EndDate
	ORDER BY HAKEMUS.CTS
	
END