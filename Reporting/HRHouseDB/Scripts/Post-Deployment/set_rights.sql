﻿-- =============================================
-- Script Template
-- =============================================
GRANT EXECUTE TO [HRHouse]
GO

GRANT SELECT TO [HRHouse]
GO

exec sp_addrolemember 'aspnet_Membership_FullAccess', 'hrhouse'

GO