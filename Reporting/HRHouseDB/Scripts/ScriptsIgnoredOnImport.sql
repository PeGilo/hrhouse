﻿
/*
Functions created by this script:
	dbo.F_START_OF_CENTURY( @DAY )
	dbo.F_START_OF_DECADE( @DAY )
	dbo.F_START_OF_YEAR( @DAY )
	dbo.F_START_OF_QUARTER( @DAY )
	dbo.F_START_OF_MONTH( @DAY )
	dbo.F_START_OF_DAY( @DAY )
	dbo.F_START_OF_HOUR( @DAY )
	dbo.F_START_OF_30_MIN( @DAY )
	dbo.F_START_OF_20_MIN( @DAY )
	dbo.F_START_OF_15_MIN( @DAY )
	dbo.F_START_OF_10_MIN( @DAY )
	dbo.F_START_OF_05_MIN( @DAY )
	dbo.F_START_OF_MINUTE( @DAY )
	dbo.F_START_OF_SECOND( @DAY )

*/

GO
if objectproperty(object_id('dbo.F_START_OF_CENTURY'),'IsScalarFunction') = 1
	begin drop function dbo.F_START_OF_CENTURY end
GO
if objectproperty(object_id('dbo.F_START_OF_DECADE'),'IsScalarFunction') = 1
	begin drop function dbo.F_START_OF_DECADE end
GO
if objectproperty(object_id('dbo.F_START_OF_YEAR'),'IsScalarFunction') = 1
	begin drop function dbo.F_START_OF_YEAR end
GO
if objectproperty(object_id('dbo.F_START_OF_QUARTER'),'IsScalarFunction') = 1
	begin drop function dbo.F_START_OF_QUARTER end
GO
if objectproperty(object_id('dbo.F_START_OF_MONTH'),'IsScalarFunction') = 1
	begin drop function dbo.F_START_OF_MONTH end
GO
if objectproperty(object_id('dbo.F_START_OF_DAY'),'IsScalarFunction') = 1
	begin drop function dbo.F_START_OF_DAY end
GO
if objectproperty(object_id('dbo.F_START_OF_HOUR'),'IsScalarFunction') = 1
	begin drop function dbo.F_START_OF_HOUR end
GO
if objectproperty(object_id('dbo.F_START_OF_30_MIN'),'IsScalarFunction') = 1
	begin drop function dbo.F_START_OF_30_MIN end
GO
if objectproperty(object_id('dbo.F_START_OF_20_MIN'),'IsScalarFunction') = 1
	begin drop function dbo.F_START_OF_20_MIN end
GO
if objectproperty(object_id('dbo.F_START_OF_15_MIN'),'IsScalarFunction') = 1
	begin drop function dbo.F_START_OF_15_MIN end
GO
if objectproperty(object_id('dbo.F_START_OF_10_MIN'),'IsScalarFunction') = 1
	begin drop function dbo.F_START_OF_10_MIN end
GO
if objectproperty(object_id('dbo.F_START_OF_05_MIN'),'IsScalarFunction') = 1
	begin drop function dbo.F_START_OF_05_MIN end
GO
if objectproperty(object_id('dbo.F_START_OF_X_MIN'),'IsScalarFunction') = 1
	begin drop function dbo.F_START_OF_X_MIN end
GO
if objectproperty(object_id('dbo.F_START_OF_MINUTE'),'IsScalarFunction') = 1
	begin drop function dbo.F_START_OF_MINUTE end
GO
if objectproperty(object_id('dbo.F_START_OF_SECOND'),'IsScalarFunction') = 1
	begin drop function dbo.F_START_OF_SECOND end

GO
