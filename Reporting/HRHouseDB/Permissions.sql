﻿GRANT EXECUTE
    ON OBJECT::[dbo].[aspnet_CheckSchemaVersion] TO [aspnet_Membership_FullAccess];


GO
GRANT EXECUTE
    ON OBJECT::[dbo].[aspnet_Membership_CreateUser] TO [aspnet_Membership_FullAccess];


GO
GRANT EXECUTE
    ON OBJECT::[dbo].[aspnet_Membership_GetPasswordWithFormat] TO [aspnet_Membership_FullAccess];


GO
GRANT CREATE TABLE TO [HRHouse];


GO
GRANT ALTER
    ON SCHEMA::[dbo] TO [HRHouse];

