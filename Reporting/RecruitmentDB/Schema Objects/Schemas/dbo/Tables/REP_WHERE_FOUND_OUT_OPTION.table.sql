﻿CREATE TABLE [dbo].[REP_WHERE_FOUND_OUT_OPTION] (
    [W_ID]               INT           NOT NULL,
    [W_NAME]             NVARCHAR (50) NULL,
    [W_DOMAIN_ID]        INT           NULL,
    [W_INACTIVE]         SMALLINT      DEFAULT ((0)) NULL,
    [W_ALWAYS_LAST]      SMALLINT      DEFAULT ((0)) NULL,
    [W_ORDER_BY]         INT           NULL,
    [W_LAYER_OTHER_WHAT] SMALLINT      DEFAULT ((0)) NULL,
    [CTS]                DATETIME      NULL,
    [UTS]                DATETIME      NULL,
    PRIMARY KEY CLUSTERED ([W_ID] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF)
);

