﻿CREATE TABLE [dbo].[REP_REKRY_TAITO] (
    [ID]                           INT            NOT NULL,
    [TAITORYHMA_ID]                INT            NULL,
    [TAITO_NIMI]                   NVARCHAR (120) NULL,
    [SKILL_UNION_ID]               INT            NULL,
    [HTML_YES_OR_NO]               SMALLINT       DEFAULT ((0)) NULL,
    [HTML_LARGE_TEXTAREA]          SMALLINT       DEFAULT ((0)) NULL,
    [DATE_COL]                     SMALLINT       DEFAULT ((0)) NULL,
    [DATE_MONTH_AND_YEARS]         SMALLINT       DEFAULT ((0)) NULL,
    [HOMEPAGE_INTERFACE_SKILL]     SMALLINT       DEFAULT ((0)) NULL,
    [NOT_SEARCHABLE_SKILL]         SMALLINT       DEFAULT ((0)) NULL,
    [NOT_IN_LANG_REPORT]           SMALLINT       DEFAULT ((0)) NULL,
    [LANGUAGE_TYPE_ID]             INT            NULL,
    [EXCLUDE_FROM_LANG_COMBO]      SMALLINT       DEFAULT ((0)) NULL,
    [NOT_IN_COPY_LIST]             SMALLINT       DEFAULT ((0)) NULL,
    [GUARDDOG_FIELD_ID]            INT            NULL,
    [SKILL_NOTES_SQL_COND_TYPE_ID] INT            NULL,
    [CTS]                          DATETIME       NULL,
    [UTS]                          DATETIME       NULL,
    PRIMARY KEY CLUSTERED ([ID] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF),
    FOREIGN KEY ([TAITORYHMA_ID]) REFERENCES [dbo].[REP_REKRY_TAITORYHMA] ([ID]) ON DELETE NO ACTION ON UPDATE NO ACTION
);

