﻿CREATE TABLE [dbo].[REP_REKRY_TAITORYHMA] (
    [ID]                            INT            NOT NULL,
    [TAITORYHMA_NIMI]               NVARCHAR (50)  NULL,
    [HTML_HEADING]                  NVARCHAR (200) NULL,
    [ABSOLUTE_SEARCH]               SMALLINT       DEFAULT ((0)) NULL,
    [SEARCH_DISABLED]               SMALLINT       DEFAULT ((0)) NULL,
    [NO_HEADING_SEARCH_VIEW]        SMALLINT       DEFAULT ((0)) NULL,
    [NOT_SEARCHABLE_SKILLGROUP]     SMALLINT       DEFAULT ((0)) NULL,
    [FIELD_ID_FOR_OPEN_APP_BASKETS] INT            NULL,
    [SKILLS_TRANSFER_ID]            INT            NULL,
    [SEARCH_ENGINE_LAYER_GROUP_ID]  INT            NULL,
    [MAIN_LAYER_TAG_NAME]           NVARCHAR (50)  NULL,
    [CTS]                           DATETIME       NULL,
    [UTS]                           DATETIME       NULL,
    PRIMARY KEY CLUSTERED ([ID] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF)
);

