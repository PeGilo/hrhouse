﻿CREATE TABLE [dbo].[REP_WORK_SHIFT] (
    [WS_ID]                     INT            NOT NULL,
    [WS_NAME]                   NVARCHAR (100) NULL,
    [WS_ORDER_BY]               INT            NULL,
    [WS_DOMAIN_ID]              INT            NULL,
    [WS_USED_IN_OVERTIME_AVAIL] SMALLINT       DEFAULT ((0)) NULL,
    [CTS]                       DATETIME       NULL,
    [UTS]                       DATETIME       NULL,
    PRIMARY KEY CLUSTERED ([WS_ID] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF)
);

