﻿CREATE TABLE [dbo].[REP_APP_JOB_TYPE] (
    [APT_APPLICATION_ID] INT      NOT NULL,
    [APT_JOB_TYPE_ID]    INT      NOT NULL,
    [APT_START_DT]       DATETIME NULL,
    [APT_END_DT]         DATETIME NULL,
    [CTS]                DATETIME NULL,
    [UTS]                DATETIME NULL,
    PRIMARY KEY CLUSTERED ([APT_APPLICATION_ID] ASC, [APT_JOB_TYPE_ID] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF),
    FOREIGN KEY ([APT_JOB_TYPE_ID]) REFERENCES [dbo].[REP_JOB_TYPE] ([TJ_ID]) ON DELETE NO ACTION ON UPDATE NO ACTION,
    FOREIGN KEY ([APT_APPLICATION_ID]) REFERENCES [dbo].[REP_REKRY_HAKEMUS] ([ID]) ON DELETE NO ACTION ON UPDATE NO ACTION
);

