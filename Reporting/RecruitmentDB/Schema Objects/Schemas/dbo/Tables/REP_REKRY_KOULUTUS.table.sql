﻿CREATE TABLE [dbo].[REP_REKRY_KOULUTUS] (
    [ID]                  INT            NOT NULL,
    [KOULUTUS_NIMI]       NVARCHAR (100) NULL,
    [KOULUTUS_COLLECTION] INT            NULL,
    [KOULUTUS_NOT_USED]   SMALLINT       DEFAULT ((0)) NULL,
    [KOULUTUS_ORDERBY]    INT            NULL,
    PRIMARY KEY CLUSTERED ([ID] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF)
);

