﻿ALTER DATABASE [$(DatabaseName)]
    ADD LOG FILE (NAME = [Recruitment_log], FILENAME = '$(DefaultDataPath)$(DatabaseName).ldf', MAXSIZE = 2097152 MB, FILEGROWTH = 10 %);

