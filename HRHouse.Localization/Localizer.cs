﻿using System;
using System.Globalization;
using System.Resources;
using System.Reflection;

namespace HRHouse.Localization
{
    public class Localizer
    {
        private ResourceManager resourceManager;

        private string resourceName { get; set; }

        public Localizer(string resourceName)
        {
            if (string.IsNullOrEmpty(resourceName))
                throw new ArgumentNullException("resourceName");

            this.resourceName = resourceName;
            Initialize();
        }

        void Initialize()
        {
            string nameSpace = this.GetType().Namespace;
            resourceManager = new ResourceManager(nameSpace + "." + this.resourceName,
                Assembly.GetExecutingAssembly());

        }

        public string GetString(string key, string culture)
        {
            if (string.IsNullOrEmpty(key))
                throw new ArgumentNullException("key");
            if (string.IsNullOrEmpty(culture))
                throw new ArgumentNullException("culture");

            return resourceManager.GetString(key,new CultureInfo(culture));
        }
    }
}
