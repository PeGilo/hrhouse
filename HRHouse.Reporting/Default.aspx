﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeBehind="Default.aspx.cs" Inherits="HRHouse.Reporting._Default" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <rsweb:ReportViewer ID="ReportViewer1" runat="server" 
        Width="100%" Height="100%" ProcessingMode="Remote">
        <ServerReport ReportPath="/Production/Menu" ReportServerUrl="http://drs3/reportserver" />
</rsweb:ReportViewer>
</asp:Content>
